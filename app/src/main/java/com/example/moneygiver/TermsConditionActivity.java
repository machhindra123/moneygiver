package com.example.moneygiver;

import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.TextView;

public class TermsConditionActivity extends AppCompatActivity {

    TextView txtView_welcm;
    SharedPreferences shpreferences;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate ( savedInstanceState );
        setContentView ( R.layout.activity_terms_condition );

        txtView_welcm = findViewById ( R.id.txtView_welcm );
        shpreferences = getApplicationContext ().getSharedPreferences ( "value",0 );
        String nuser = shpreferences.getString ( "NAME" ,null);
        txtView_welcm.setText ( "Welcome " + nuser );
    }
}
