package com.example.moneygiver;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputLayout;

public class Agentlogin_Activity extends AppCompatActivity implements View.OnClickListener {

    TextInputLayout textinputLayoutAgentUser,textinputLayoutAgentPass;
    EditText agent_mobileNo,agentPass;
    TextView agent_forgotPassword,agent_register;
    Button agentlogin;
    DBHelper1 db;
    ScrollView scroll_view;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agentlogin_);

        textinputLayoutAgentUser=findViewById ( R.id.textinputLayoutAgentUser );
        textinputLayoutAgentPass=findViewById ( R.id.textinputLayoutAgentPass );
        agent_mobileNo=findViewById(R.id.agent_mobileNo);
        agentPass=findViewById(R.id.agentPass);
        agent_forgotPassword=findViewById(R.id.agent_forgotPassword);
        agent_register=findViewById(R.id.agent_register);
        agentlogin=findViewById(R.id.agentlogin);
        scroll_view = findViewById ( R.id.scroll_view );
        agent_register.setOnClickListener ( this );
        agentlogin.setOnClickListener ( this );
        agent_forgotPassword.setOnClickListener ( this );
        db = new DBHelper1 ( this );

    }

    private void Agent_login() {
        String agUser_Id = agent_mobileNo.getText().toString().trim ();
        String ag_Pass = agentPass.getText().toString().trim ();

        if (!agUser_Id.equals("")&& !ag_Pass.equals("")) {

            if (db.checkUserAgent ( agUser_Id, ag_Pass )) {

                Intent in = new Intent ( this, Welcome_AgentActivity.class );
                emptyInputEditText ();
                startActivity ( in );
            } else if (!db.checkUserAgent ( agUser_Id, ag_Pass )) {
                Toast.makeText ( this, "Enter correct User Name or Password", Toast.LENGTH_SHORT ).show ();
            } else {
                // Snack Bar to show success message that record is wrong
                Snackbar.make ( scroll_view, getString ( R.string.error_valid_email_password ), Snackbar.LENGTH_LONG ).show ();
            }
        }
            else {
                if (agUser_Id.equals ( "" )) {
                    Toast.makeText ( this, "Please Enter User Name", Toast.LENGTH_SHORT ).show ();
                } else if (ag_Pass.equals ( "" )) {
                    Toast.makeText ( this, "Please Enter Password", Toast.LENGTH_SHORT ).show ();
                }
            }
        }
    /**
     * This method is to empty all input edit text
     */
    private void emptyInputEditText() {
        agent_mobileNo.setText(null);
        agentPass.setText(null);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent i = new Intent(this, DBHelper1.class);
        startActivity(i);
        finish();
    }
    @Override
    public void onClick(View view) {
        switch (view.getId ()){
            case R.id.agent_register:
                startActivity(new Intent(this, Agent_RegistrationActivity.class));
                break;
            case R.id.agentlogin:
                //startActivity(new Intent(this, Welcome_AgentActivity.class));
                Agent_login();
                break;
            case R.id.agent_forgotPassword:
                startActivity ( new Intent ( this,ForgotPass_AgentActivity.class ) );
                break;
        }
    }
}
