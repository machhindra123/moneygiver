package com.example.moneygiver;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.moneygiver.Model.AgentModel;
import com.example.moneygiver.Model.FunderModel;
import com.example.moneygiver.Model.UserModel;

import java.util.ArrayList;
import java.util.List;

public class DBHelper1 extends SQLiteOpenHelper {

    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "UserManager.db";

    // User table name
    private static final String TABLE_NAME = "user";
    private static final String TABLE_FUNDERNAME = "user_funder";
    private static final String TABLE_AGENTNAME = "user_agent";

    // User Table Columns names
    private static final String COLUMN_ID = "id";
    private static final String COLUMN_CODE = "agent_code";
    private static final String COLUMN_NAME = "user_name";
    private static final String COLUMN_ADDRESS = "user_address";
    private static final String COLUMN_MOBILE = "user_mobile";
    private static final String COLUMN_BIRTHDAY = "user_birthday";
    private static final String COLUMN_SEX = "user_sex";
    private static final String COLUMN_EMAIL = "user_email";
    private static final String COLUMN_ADHAR = "user_adhar";
    private static final String COLUMN_PAN = "user_pan";
    /*private static final String COLUMN_AGENT_NAME = "user_name";
    private static final String COLUMN_AGENT_ADDRESS = "user_address";
    private static final String COLUMN_AGENT_MOBILE = "user_mobile";
    private static final String COLUMN_AGENT_BIRTHDAY = "user_birthday";
    private static final String COLUMN_AGENT_EMAIL = "user_email";
    private static final String COLUMN_AGENT_ADHAR = "user_adhar";
    private static final String COLUMN_AGENT_PAN = "user_pan";*/
    private static final String COLUMN_BANKNAME = "user_bankname";
    private static final String COLUMN_ACCOUNT = "user_account";
    private static final String COLUMN_BRANCH = "user_branch";
    private static final String COLUMN_IFSC = "user_ifsc";

    // create table sql query
    private String CREATE_USER_TABLE = "CREATE TABLE " + TABLE_NAME + "(" + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + COLUMN_NAME + " TEXT," + COLUMN_ADDRESS + " TEXT,"
            + COLUMN_MOBILE + " NUMBER," + COLUMN_BIRTHDAY + " TEXT," + COLUMN_SEX + " TEXT," + COLUMN_EMAIL + " TEXT," +
             COLUMN_ADHAR + " TEXT," + COLUMN_PAN + " TEXT" + ")";

    // create table sql query
    private String CREATE_FUNDER_TABLE = "CREATE TABLE " + TABLE_FUNDERNAME + "(" + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + COLUMN_CODE + " TEXT," + COLUMN_NAME + " TEXT," + COLUMN_ADDRESS + " TEXT,"
            + COLUMN_MOBILE + " NUMBER," + COLUMN_BIRTHDAY + " TEXT," + COLUMN_SEX + " TEXT," + COLUMN_EMAIL + " TEXT," +
            COLUMN_ADHAR + " TEXT," + COLUMN_PAN + " TEXT" + ")";
    // create table sql query
    private String CREATE_AGENT_TABLE = "CREATE TABLE " + TABLE_AGENTNAME + "(" + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + COLUMN_CODE + " TEXT," + COLUMN_NAME + " TEXT," + COLUMN_ADDRESS + " TEXT,"
            + COLUMN_MOBILE + " NUMBER," + COLUMN_BIRTHDAY + " TEXT," + COLUMN_SEX + " TEXT," + COLUMN_EMAIL + " TEXT," +
            COLUMN_ADHAR + " TEXT," + COLUMN_PAN + " TEXT," + COLUMN_BANKNAME + " TEXT," + COLUMN_ACCOUNT + " TEXT," +
            COLUMN_BRANCH + " TEXT," + COLUMN_IFSC + " TEXT" + ")";
    // drop table sql query
    private String DROP_USER_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME;
    private String DROP_FUNDER_TABLE = "DROP TABLE IF EXISTS " + TABLE_FUNDERNAME;
    private String DROP_AGENT_TABLE = "DROP TABLE IF EXISTS " + TABLE_AGENTNAME;

    public DBHelper1(Context context) {
        super(context,DATABASE_NAME,null,DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_USER_TABLE);
        db.execSQL(CREATE_FUNDER_TABLE);
        db.execSQL(CREATE_AGENT_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        //Drop User Table if exist
        db.execSQL(DROP_USER_TABLE);
        db.execSQL(DROP_FUNDER_TABLE);
        db.execSQL(DROP_AGENT_TABLE);

        // Create tables again
        onCreate(db);
    }
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }
    // Put information into a database--------
    public void addUser(UserModel userModel) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMN_NAME, userModel.getName ());
        values.put(COLUMN_ADDRESS, userModel.getAdress ());
        values.put(COLUMN_MOBILE, userModel.getMobile ());
        values.put(COLUMN_BIRTHDAY, userModel.getBirthday ());
        values.put(COLUMN_SEX, userModel.getSex ());
        values.put(COLUMN_EMAIL, userModel.getEmail ());
        values.put(COLUMN_ADHAR, userModel.getAdhar ());
        values.put(COLUMN_PAN, userModel.getPan ());

        // Inserting Row
        db.insert(TABLE_NAME, null, values);
        // close db connection
        db.close();
    }
    // Put information into a database--------
    public void addUser(FunderModel funderModel) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMN_CODE, funderModel.getFund_code ());
        values.put(COLUMN_NAME, funderModel.getFund_name ());
        values.put(COLUMN_ADDRESS, funderModel.getFund_adress ());
        values.put(COLUMN_MOBILE, funderModel.getFund_mobile ());
        values.put(COLUMN_BIRTHDAY, funderModel.getFund_birthday ());
        values.put(COLUMN_SEX, funderModel.getFund_sex ());
        values.put(COLUMN_EMAIL, funderModel.getFund_email ());
        values.put(COLUMN_ADHAR, funderModel.getFund_adhar ());
        values.put(COLUMN_PAN, funderModel.getFund_pan ());

        // Inserting Row
        db.insert(TABLE_FUNDERNAME, null, values);
        // close db connection
        db.close();
    }
    // Put information into a database--------
    public void addUser(AgentModel agentModel) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMN_CODE, agentModel.getAgen_code ());
        values.put(COLUMN_NAME, agentModel.getAgen_name ());
        values.put(COLUMN_ADDRESS, agentModel.getAgen_adress ());
        values.put(COLUMN_MOBILE, agentModel.getAgen_mobile ());
        values.put(COLUMN_BIRTHDAY, agentModel.getAgen_birthday ());
        values.put(COLUMN_SEX, agentModel.getAgen_sex ());
        values.put(COLUMN_EMAIL, agentModel.getAgen_email ());
        values.put(COLUMN_ADHAR, agentModel.getAgen_adhar ());
        values.put(COLUMN_PAN, agentModel.getAgen_pan ());
        values.put(COLUMN_BANKNAME, agentModel.getAgen_bankname ());
        values.put(COLUMN_ACCOUNT, agentModel.getAgen_bankacc ());
        values.put(COLUMN_BRANCH, agentModel.getAgen_bankbranch ());
        values.put(COLUMN_IFSC, agentModel.getAgen_bankifsc ());

        // Inserting Row
        db.insert(TABLE_AGENTNAME, null, values);
        // close db connection
        db.close();
    }

    // code to get all users in a list view
    public List<UserModel> getAllUser() {

        List<UserModel> userList = new ArrayList<UserModel>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_NAME;

        SQLiteDatabase db = this.getReadableDatabase ();

        Cursor cursor = db.rawQuery(selectQuery,null);

        if (cursor.moveToFirst()) {
            do {
                UserModel user = new UserModel();
                user.setName(cursor.getString(cursor.getColumnIndex ( COLUMN_NAME )));
                user.setAdress(cursor.getString(cursor.getColumnIndex(COLUMN_ADDRESS)));
                user.setMobile(cursor.getString(cursor.getColumnIndex(COLUMN_MOBILE)));
                user.setBirthday(cursor.getString(cursor.getColumnIndex(COLUMN_BIRTHDAY)));
                user.setSex (cursor.getString(cursor.getColumnIndex(COLUMN_SEX)));
                user.setEmail(cursor.getString(cursor.getColumnIndex(COLUMN_EMAIL)));
                user.setAdhar(cursor.getString(cursor.getColumnIndex(COLUMN_ADHAR)));
                user.setPan(cursor.getString(cursor.getColumnIndex(COLUMN_PAN)));
                // Adding user record to list
                userList.add(user);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();

        // return user list
        return userList;
    }
    // code to get all users in a list view
    public List<FunderModel> getAllFunderUser() {

        List<FunderModel> funderList = new ArrayList<FunderModel>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_FUNDERNAME;

        SQLiteDatabase db = this.getReadableDatabase ();

        Cursor cursor = db.rawQuery(selectQuery,null);

        if (cursor.moveToFirst()) {
            do {
                FunderModel funderModel = new FunderModel();
                funderModel.setFund_code ( cursor.getString ( cursor.getColumnIndex ( COLUMN_CODE ) ) );
                funderModel.setFund_name (cursor.getString(cursor.getColumnIndex ( COLUMN_NAME )));
                funderModel.setFund_adress (cursor.getString(cursor.getColumnIndex(COLUMN_ADDRESS)));
                funderModel.setFund_mobile (cursor.getString(cursor.getColumnIndex(COLUMN_MOBILE)));
                funderModel.setFund_birthday (cursor.getString(cursor.getColumnIndex(COLUMN_BIRTHDAY)));
                funderModel.setFund_sex (cursor.getString(cursor.getColumnIndex(COLUMN_SEX)));
                funderModel.setFund_email (cursor.getString(cursor.getColumnIndex(COLUMN_EMAIL)));
                funderModel.setFund_adhar (cursor.getString(cursor.getColumnIndex(COLUMN_ADHAR)));
                funderModel.setFund_pan (cursor.getString(cursor.getColumnIndex(COLUMN_PAN)));
                // Adding user record to list
                funderList.add(funderModel);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();

        // return user list
        return funderList;
    }
    // code to get all users in a list view
    public List<AgentModel> getAllAgentUser() {

        List<AgentModel> agentList = new ArrayList<AgentModel>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_AGENTNAME;

        SQLiteDatabase db = this.getReadableDatabase ();

        Cursor cursor = db.rawQuery(selectQuery,null);

        if (cursor.moveToFirst()) {
            do {
                AgentModel agentModel = new AgentModel();
                agentModel.setAgen_code ( cursor.getString ( cursor.getColumnIndex ( COLUMN_CODE ) ) );
                agentModel.setAgen_name (cursor.getString(cursor.getColumnIndex ( COLUMN_NAME )));
                agentModel.setAgen_adress (cursor.getString(cursor.getColumnIndex(COLUMN_ADDRESS)));
                agentModel.setAgen_mobile (cursor.getString(cursor.getColumnIndex(COLUMN_MOBILE)));
                agentModel.setAgen_birthday (cursor.getString(cursor.getColumnIndex(COLUMN_BIRTHDAY)));
                agentModel.setAgen_sex ( Integer.parseInt ( cursor.getString(cursor.getColumnIndex(COLUMN_SEX)) ) );
                agentModel.setAgen_email (cursor.getString(cursor.getColumnIndex(COLUMN_EMAIL)));
                agentModel.setAgen_adhar (cursor.getString(cursor.getColumnIndex(COLUMN_ADHAR)));
                agentModel.setAgen_pan (cursor.getString(cursor.getColumnIndex(COLUMN_PAN)));
                // Adding user record to list
                agentList.add(agentModel);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();

        // return user list
        return agentList;
    }


    public void updateUser(UserModel user) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMN_NAME, user.getName());
        values.put(COLUMN_ADDRESS, user.getAdress());
        values.put(COLUMN_MOBILE, user.getMobile());
        values.put(COLUMN_BIRTHDAY, user.getBirthday());
        values.put(COLUMN_EMAIL, user.getEmail());
        values.put(COLUMN_ADHAR, user.getAdhar());
        values.put(COLUMN_PAN, user.getPan());

        // updating row
        db.update(TABLE_NAME, values, COLUMN_ID + " = ?",
                new String[]{String.valueOf(user.getId ())});
        db.close();
    }

    public void deleteUser(UserModel user) {
        SQLiteDatabase db = this.getWritableDatabase();
        // delete user record by id
        db.delete(TABLE_NAME, COLUMN_ID + " = ?",
                new String[]{String.valueOf(user.getId ())});
        db.close();
    }
 //This method to check user exist or not
    public boolean checkUser(String email) {

        // array of columns to fetch
        String[] columns = { COLUMN_ID };
        SQLiteDatabase db = this.getReadableDatabase ();

        // selection criteria
        String selection = COLUMN_EMAIL + " = ?";
        // selection argument
        String[] selectionArgs = {email};

        // query user table with condition
        /**
         * Here query function is used to fetch records from user table this function works like we use sql query.
         * SQL query equivalent to this query function is
         * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
         */
        //String selectquery="SELECT * FROM TABLE_NAME";
        Cursor cursor = db.query(TABLE_NAME, //Table to query
                columns,                    //columns to return
                selection,                  //columns for the WHERE clause
                selectionArgs,              //The values for the WHERE clause
                null,                       //group the rows
                null,                      //filter by row groups
                null,                   //The sort order
                null);
        int cursorCount = cursor.getCount();
        cursor.close();
        db.close();

        if (cursorCount > 0) {
            return true;
        }
            return false;
    }
    //This method to check user exist or not
    public boolean checkUserFunder(String email) {

        // array of columns to fetch
        String[] columns = { COLUMN_ID };
        SQLiteDatabase db = this.getReadableDatabase ();

        // selection criteria
        String selection = COLUMN_EMAIL + " = ?";
        // selection argument
        String[] selectionArgs = {email};

        // query user table with condition
        /**
         * Here query function is used to fetch records from user table this function works like we use sql query.
         * SQL query equivalent to this query function is
         * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
         */
        //String selectquery="SELECT * FROM TABLE_NAME";
        Cursor cursor = db.query(TABLE_FUNDERNAME, //Table to query
                columns,                    //columns to return
                selection,                  //columns for the WHERE clause
                selectionArgs,              //The values for the WHERE clause
                null,                       //group the rows
                null,                      //filter by row groups
                null,                   //The sort order
                null);
        int cursorCount = cursor.getCount();
        cursor.close();
        db.close();

        if (cursorCount > 0) {
            return true;
        }
        return false;
    }
    //This method to check user exist or not
    public boolean checkUserAgent(String email) {

        // array of columns to fetch
        String[] columns = { COLUMN_ID };
        SQLiteDatabase db = this.getReadableDatabase ();

        // selection criteria
        String selection = COLUMN_EMAIL + " = ?";
        // selection argument
        String[] selectionArgs = {email};

        // query user table with condition
        /**
         * Here query function is used to fetch records from user table this function works like we use sql query.
         * SQL query equivalent to this query function is
         * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
         */
        //String selectquery="SELECT * FROM TABLE_NAME";
        Cursor cursor = db.query(TABLE_AGENTNAME, //Table to query
                columns,                    //columns to return
                selection,                  //columns for the WHERE clause
                selectionArgs,              //The values for the WHERE clause
                null,                       //group the rows
                null,                      //filter by row groups
                null,                   //The sort order
                null);
        int cursorCount = cursor.getCount();
        cursor.close();
        db.close();

        if (cursorCount > 0) {
            return true;
        }
        return false;
    }

     // This method to check user exist or not

    public boolean checkUser(String mobile, String name) {

        // array of columns to fetch
        String[] columns = { COLUMN_ID };

        SQLiteDatabase db = this.getReadableDatabase ();
        // selection criteria
        String selection = COLUMN_MOBILE + " = ?" + " AND " + COLUMN_NAME + " = ?";

        // selection arguments
        String[] selectionArgs = {mobile, name};
        // query user table with conditions
        /**
         * Here query function is used to fetch records from user table this function works like we use sql query.
         * SQL query equivalent to this query function is
         * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
         */
        Cursor cursor = db.query(TABLE_NAME, //Table to query
                columns,                    //columns to return
                selection,                  //columns for the WHERE clause
                selectionArgs,              //The values for the WHERE clause
                null,                       //group the rows
                null,                       //filter by row groups
                null);                      //The sort order

        int cursorCount = cursor.getCount();

        cursor.close();
        db.close();
        if (cursorCount > 0) {
            return true;
        }
            return false;
    }
    // This method to check user exist or not

    public boolean checkUserFunder(String mobile, String funname) {

        // array of columns to fetch
        String[] columns = { COLUMN_ID };

        SQLiteDatabase db = this.getReadableDatabase ();
        // selection criteria
        String selection = COLUMN_MOBILE + " = ?" + " AND " + COLUMN_NAME + " = ?";

        // selection arguments
        String[] selectionArgs = {mobile, funname};
        // query user table with conditions
        /**
         * Here query function is used to fetch records from user table this function works like we use sql query.
         * SQL query equivalent to this query function is
         * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
         */
        Cursor cursor = db.query(TABLE_FUNDERNAME, //Table to query
                columns,                    //columns to return
                selection,                  //columns for the WHERE clause
                selectionArgs,              //The values for the WHERE clause
                null,                       //group the rows
                null,                       //filter by row groups
                null);                      //The sort order

        int cursorCount = cursor.getCount();

        cursor.close();
        db.close();
        if (cursorCount > 0) {
            return true;
        }
        return false;
    }
    // This method to check user exist or not

    public boolean checkUserAgent(String mobile, String agetname) {

        // array of columns to fetch
        String[] columns = { COLUMN_ID };

        SQLiteDatabase db = this.getReadableDatabase ();
        // selection criteria
        String selection = COLUMN_MOBILE + " = ?" + " AND " + COLUMN_NAME + " = ?";

        // selection arguments
        String[] selectionArgs = {mobile, agetname};
        // query user table with conditions
        /**
         * Here query function is used to fetch records from user table this function works like we use sql query.
         * SQL query equivalent to this query function is
         * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
         */
        Cursor cursor = db.query(TABLE_AGENTNAME, //Table to query
                columns,                    //columns to return
                selection,                  //columns for the WHERE clause
                selectionArgs,              //The values for the WHERE clause
                null,                       //group the rows
                null,                       //filter by row groups
                null);                      //The sort order

        int cursorCount = cursor.getCount();

        cursor.close();
        db.close();
        if (cursorCount > 0) {
            return true;
        }
        return false;
    }
 }
