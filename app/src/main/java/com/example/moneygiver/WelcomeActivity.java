package com.example.moneygiver;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class WelcomeActivity extends AppCompatActivity implements View.OnClickListener {

    Button btnloanApp,btnEnquiry,btnCondition,btnchpass,btnlogout;
    TextView txtview_welcome;
    String string = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        btnloanApp = findViewById(R.id.btnloanApp);
        btnEnquiry = findViewById(R.id.btnEnquiry);
        btnCondition = findViewById(R.id.btnCondition);
        btnchpass = findViewById(R.id.btnchpass);
        btnlogout = findViewById(R.id.btnlogout);
        txtview_welcome=findViewById ( R.id.txtview_welcome );

        SharedPreferences pref = getApplicationContext ().getSharedPreferences ( "value",0 );
        String name = pref.getString ( "NAME" ,null);
        txtview_welcome.setText ( "Welcome " + name );

 //  Apply setOnClickListener On Buttons
        btnloanApp.setOnClickListener ( this );
        btnEnquiry.setOnClickListener ( this );
        btnCondition.setOnClickListener ( this );
        btnchpass.setOnClickListener ( this );
        btnlogout.setOnClickListener ( this );
    }

    @Override
    public void onClick(View view) {
        switch (view.getId ()) {
            case R.id.btnloanApp:
                startActivity ( new Intent ( this,LoanApplication_Activity.class ) );
                break;
            case R.id.btnEnquiry:
                startActivity ( new Intent ( this,BankEnquiry.class ) );
                break;
            case R.id.btnCondition:
                startActivity ( new Intent ( this,TermsConditionActivity.class ) );
                break;
            case R.id.btnchpass:
                startActivity ( new Intent ( this,PassChangeActivity.class ) );
                break;
            case R.id.btnlogout:
                startActivity ( new Intent ( this,Userlogin_Activity.class ) );
                break;
        }
    }
}
