package com.example.moneygiver;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ScrollView;
import android.widget.TextView;

import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputLayout;

public class FunderInvestment_Activity extends AppCompatActivity {

    TextView txtviewcode,txtviewName,txtId,txtviewDOB;
    TextInputLayout tilamtInvest;
    Button btn_investment;
    ScrollView scrollview;
    SharedPreferences shpf;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate ( savedInstanceState );
        setContentView ( R.layout.activity_funder_investment_ );

        scrollview = findViewById ( R.id.scrollview );
        txtviewcode = findViewById ( R.id.txtviewcode );
        txtviewName = findViewById ( R.id.txtviewName );
        txtId = findViewById ( R.id.txtId );
        txtviewDOB = findViewById ( R.id.txtviewDOB );
        tilamtInvest = findViewById ( R.id.tilamtInvest );
        btn_investment = findViewById ( R.id.btn_investment );

        shpf = getApplicationContext ().getSharedPreferences ( "value_funder",0 );
        String code = String.valueOf ( shpf.getLong ( "codefunder",0 ) );
        txtviewcode.setText ( code );
        String fundername = shpf.getString ( "FNAME",null );
        txtviewName.setText ( fundername );

        btn_investment.setOnClickListener ( new View.OnClickListener () {
            @Override
            public void onClick(View view) {
                String investAmt = tilamtInvest.getEditText ().getText ().toString ().trim ();
                if ( !investAmt.isEmpty ()) {
                    Intent intent = new Intent ( FunderInvestment_Activity.this, FunderInvestmentPayment_Activity.class );
                   // intent.putExtra ( "Amount", investAmt );
                    startActivity ( intent );
                }
                else if (investAmt.isEmpty ()){
                    Snackbar.make ( scrollview,"Enter Amount ",Snackbar.LENGTH_SHORT ).show ();
                }
            }
        } );

    }
}
