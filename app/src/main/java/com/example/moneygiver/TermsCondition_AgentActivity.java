package com.example.moneygiver;

import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.TextView;

public class TermsCondition_AgentActivity extends AppCompatActivity {

    TextView txtview_wel;
    SharedPreferences shapre;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate ( savedInstanceState );
        setContentView ( R.layout.activity_terms_condition_agent );

        txtview_wel = findViewById ( R.id.txtview_wel );

        shapre = getApplicationContext ().getSharedPreferences ( "value_agent",0 );
        String nm = shapre.getString ( "ANAME",null );
        txtview_wel.setText ( "Welcome " + nm );
    }
}
