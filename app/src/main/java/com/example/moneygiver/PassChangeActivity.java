package com.example.moneygiver;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputLayout;

public class PassChangeActivity extends AppCompatActivity {

    TextView txtview_code,txtview_Name;
    TextInputLayout tilCurrentPass,tilPassNew;
    ScrollView scrolView;
    Button btn_submit;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate ( savedInstanceState );
        setContentView ( R.layout.activity_pass_change );

        txtview_code = findViewById ( R.id.txtview_code );
        txtview_Name = findViewById ( R.id.txtview_Name );
        tilCurrentPass = findViewById ( R.id.tilCurrentPass );
        tilPassNew = findViewById ( R.id.tilPassNew );
        scrolView = findViewById ( R.id.scrolView );
        btn_submit = findViewById ( R.id.btn_submit );
        SharedPreferences shpref = getApplicationContext ().getSharedPreferences ( "value",0 );
        String code = String.valueOf ( shpref.getLong ( "code",0 ) );
        txtview_code.setText ( code );
        String nam = shpref.getString ( "NAME",null );
        txtview_Name.setText ( nam );
        btn_submit.setOnClickListener ( new View.OnClickListener () {
            @Override
            public void onClick(View view) {
                ChangePassword();
            }
        } );
    }

    private void ChangePassword(){
      String currentPass = tilCurrentPass.getEditText ().getText ().toString ().trim ();
      String newPass = tilPassNew.getEditText ().getText ().toString ().trim ();

           if (!currentPass.isEmpty () &&!newPass.isEmpty () ){
                    Intent i = new Intent ( PassChangeActivity.this,Userlogin_Activity.class );
                    startActivity ( i );
              }

           else if (currentPass.isEmpty ()){
               Snackbar.make ( scrolView,"Please enter current password",Snackbar.LENGTH_SHORT ).show ();
              }
           else if (newPass.isEmpty ()){
               Snackbar.make ( scrolView,"Enter a new password",Snackbar.LENGTH_SHORT ).show ();
              }
            }
}
