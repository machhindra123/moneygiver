package com.example.moneygiver;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class Welcome_FunderActivity extends AppCompatActivity implements View.OnClickListener {

    Button btnInvestMoney,btnMyInvestment,btnCondition,btnchpass,btnlogout;
    TextView txtview_welcome;
    SharedPreferences sharedpre;
    String string ="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate ( savedInstanceState );
        setContentView ( R.layout.activity_welcome_funder );
        //get Id's
        txtview_welcome=findViewById ( R.id.txtview_welcome );
        btnInvestMoney = findViewById(R.id.btnInvestMoney);
        btnMyInvestment = findViewById(R.id.btnMyInvestment);
        btnCondition = findViewById(R.id.btnCondition);
        btnchpass = findViewById(R.id.btnchpass);
        btnlogout = findViewById(R.id.btnlogout);

        sharedpre = getApplicationContext ().getSharedPreferences ( "value_funder",0 );
        String nf = sharedpre.getString ( "FNAME",null );
        txtview_welcome.setText ( "Welcome " + nf );

        //Apply setOnClickListener On Buttons
        btnInvestMoney.setOnClickListener ( this );
        btnMyInvestment.setOnClickListener ( this );
        btnCondition.setOnClickListener ( this );
        btnchpass.setOnClickListener ( this );
        btnlogout.setOnClickListener ( this );
    }

    @Override
    public void onClick(View view) {
        switch (view.getId ()) {
            case R.id.btnInvestMoney:
                startActivity ( new Intent ( this,FunderInvestment_Activity.class ) );
                break;
            case R.id.btnMyInvestment:
                startActivity ( new Intent ( this,ViewInvestment_FunderActivity.class ) );
                break;
            case R.id.btnCondition:
                startActivity ( new Intent ( this,TermsCondition_FunderActivity.class ) );
                break;
            case R.id.btnchpass:
                startActivity ( new Intent ( this,PassChange_FunderActivity.class ) );
                break;
            case R.id.btnlogout:
                startActivity ( new Intent ( this,Funderlogin_Activity.class ) );
                break;
        }
    }
}
