package com.example.moneygiver;

import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.TextView;

public class TermsCondition_FunderActivity extends AppCompatActivity {

    TextView txtView_name;
    SharedPreferences sharedPref;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate ( savedInstanceState );
        setContentView ( R.layout.activity_terms_condition_funder );

        txtView_name = findViewById ( R.id.txtView_name );
        sharedPref = getApplicationContext ().getSharedPreferences ( "value_funder",0 );
        String nm = sharedPref.getString ( "FNAME",null );
        txtView_name.setText (  "Welcome " + nm  );
    }
}
