package com.example.moneygiver;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputLayout;

public class BankEnquiry extends AppCompatActivity {

    TextInputLayout tilDesc;
    TextView txtviewCode,txtviewName,txtview_Id,txtview_Date;
    EditText edt_Desc;
    Button btn_submit;
    Spinner spinner_bank;
    SharedPreferences sprefs;
    ScrollView scrview;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate ( savedInstanceState );
        setContentView ( R.layout.activity_bank_enquiry );
        scrview = findViewById ( R.id.scrview );
        tilDesc = findViewById ( R.id.tilDesc );
        txtviewCode = findViewById ( R.id.txtviewCode );
        txtviewName = findViewById ( R.id.txtviewName );
        txtview_Id = findViewById ( R.id.txtview_Id );
        txtview_Date = findViewById ( R.id.txtview_Date );
       // edt_Desc = findViewById ( R.id.edt_Desc );
        spinner_bank = findViewById ( R.id.spinner_bank );
        btn_submit = findViewById ( R.id.btn_submit );
        sprefs = getApplicationContext ().getSharedPreferences ( "value",0 );
        String code = String.valueOf ( sprefs.getLong ( "code",0 ) );
        txtviewCode.setText ( code );
        String n = sprefs.getString ( "NAME",null );
        txtviewName.setText ( n );
        Banks();
        btn_submit.setOnClickListener ( new View.OnClickListener () {
            @Override
            public void onClick(View view) {
                input_validation();
            }
        } );

    }

    private void input_validation() {
        String desc = tilDesc.getEditText().getText ().toString ().trim ();
        String bank = spinner_bank.getSelectedItem ().toString ().trim ();
       if ( !desc.isEmpty () && !bank.equals ( "Select Bank" ) ){
           Intent it = new Intent ( BankEnquiry.this,LoanApplication_Activity.class );
           startActivity ( it );
       }
       else if (desc.isEmpty ()){
           Snackbar.make ( scrview,"Enter Description",Snackbar.LENGTH_SHORT ).show ();
       }
       else if (bank.equals ( "Select Bank" )){
           Snackbar.make ( scrview,"Select Bank",Snackbar.LENGTH_SHORT ).show ();
       }
    }

    private void Banks() {
        // Creating adapter for spinner
        ArrayAdapter<CharSequence> myadapter = ArrayAdapter.createFromResource ( this,R.array.select_bank,android.R.layout.simple_spinner_item );

        // Drop down layout style - list view with radio button
        myadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spinner_bank.setAdapter(myadapter);

        // Spinner click listener
        spinner_bank.setOnItemSelectedListener( new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    // On selecting a spinner item
                    //  parent.getItemAtPosition ( position ).toString ();
                    //set color of drop down textview
                    ((TextView) parent.getChildAt ( 0 )).setTextColor ( Color.parseColor ( "#5CD108" ) );

                }
                else {
                    // On selecting a spinner item
                    String item = parent.getItemAtPosition ( position ).toString ();

                    //set color of drop down textview
                    ((TextView) parent.getChildAt ( 0 )).setTextColor ( Color.parseColor ( "#5CD108" ) );

                    // Showing selected spinner item
                    Toast.makeText ( parent.getContext (), "Selected: " + item, Toast.LENGTH_LONG ).show ();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // showToast(q.name + " unselected");
            }
        });
    }

}
