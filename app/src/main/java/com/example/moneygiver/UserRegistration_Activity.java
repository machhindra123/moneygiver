package com.example.moneygiver;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.widget.NestedScrollView;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Patterns;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.moneygiver.JavaMailAPI.JavaMailAPI;
import com.example.moneygiver.Model.UserModel;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class UserRegistration_Activity extends AppCompatActivity {

    TextInputLayout tiluserName, tiluserAddress, tiluserMobile, tiluserBirthday,
            tiluserEmail, tiluserAdhar, tiluserPan;
    TextView txtview_userCode;
    Button btnregsubmit;
    CheckBox chkAgree;
    DBHelper1 DB1;
    ScrollView scrollView;
    Spinner spinner_gender;
    DatePickerDialog gdatePickerdialog;
    String name, adress, mobile, birthday, email, adhar, pan, Spnr_gender, password;
    private String userCode;
    SharedPreferences sharedpref;
    FirebaseAuth fAuth;
    int count = 0;
    long user_code = 3000;
    private final static int SEND_SMS_PERMISSION_REQ = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate ( savedInstanceState );
        setContentView ( R.layout.activity_user_registration_ );

        tiluserName = findViewById ( R.id.tiluserName );
        tiluserAddress = findViewById ( R.id.tiluserAddress );
        tiluserMobile = findViewById ( R.id.tiluserMobile );
        tiluserBirthday = findViewById ( R.id.tiluserBirthday );
        tiluserEmail = findViewById ( R.id.tiluserEmail );
        tiluserAdhar = findViewById ( R.id.tiluserAdhar );
        tiluserPan = findViewById ( R.id.tiluserPan );
        txtview_userCode = findViewById ( R.id.txtview_userCode );
        scrollView = findViewById ( R.id.scrollView );

        spinner_gender = findViewById ( R.id.spinner_gender );
        chkAgree = findViewById ( R.id.chkAgree );
        btnregsubmit = findViewById ( R.id.btnregsubmit );

        setDateField ();
        SpinnerGender ();
        split_string ();
        character_digits ();
        fAuth = FirebaseAuth.getInstance ();


        btnregsubmit.setOnClickListener ( new View.OnClickListener () {
            @Override
            public void onClick(View view) {
                // registerValidation();
                name = tiluserName.getEditText ().getText ().toString ().trim ();
                adress = tiluserAddress.getEditText ().getText ().toString ().trim ();
                mobile = tiluserMobile.getEditText ().getText ().toString ().trim ();
                birthday = tiluserBirthday.getEditText ().getText ().toString ().trim ();
                email = tiluserEmail.getEditText ().getText ().toString ().trim ();
                adhar = tiluserAdhar.getEditText ().getText ().toString ().trim ();
                pan = tiluserPan.getEditText ().getText ().toString ().trim ();
                Spnr_gender = spinner_gender.getSelectedItem ().toString ().trim ();

                if (name.equals ( "" )) {
                    Toast.makeText ( UserRegistration_Activity.this, "Please Enter Name", Toast.LENGTH_SHORT ).show ();
                } else if (adress.equals ( "" )) {
                    Toast.makeText ( UserRegistration_Activity.this, "Please Enter Address", Toast.LENGTH_SHORT ).show ();
                } else if (mobile.equals ( "" )) {
                    Toast.makeText ( UserRegistration_Activity.this, "Please Enter Mobile Number", Toast.LENGTH_SHORT ).show ();
                } else if (birthday.equals ( "" )) {
                    Toast.makeText ( UserRegistration_Activity.this, "Please Enter Date of Birth", Toast.LENGTH_SHORT ).show ();
                } else if (Spnr_gender.equals ( "Gender" )) {
                    Toast.makeText ( UserRegistration_Activity.this, "Please Select Gender", Toast.LENGTH_SHORT ).show ();
                } else if (email.equals ( "" ) && !Patterns.EMAIL_ADDRESS.matcher ( email ).matches ()) {
                    Toast.makeText ( UserRegistration_Activity.this, "Enter Valid Email Address", Toast.LENGTH_SHORT ).show ();
                } else if (adhar.equals ( "" )) {
                    Toast.makeText ( UserRegistration_Activity.this, "Please Enter Adhar Number", Toast.LENGTH_SHORT ).show ();
                } else if (pan.equals ( "" )) {
                    Toast.makeText ( UserRegistration_Activity.this, "Please Enter Pan Number", Toast.LENGTH_SHORT ).show ();
                } else if (chkAgree.isChecked () == false) {
                    Toast.makeText ( UserRegistration_Activity.this, "Please Click Terms & Conditions", Toast.LENGTH_SHORT ).show ();
                } else {
//                        sharedpref = getApplicationContext ().getSharedPreferences ( "value",0 );
//                        SharedPreferences.Editor editor = sharedpref.edit ();
//                        editor.putString ("NAME", name );
                    String smsmessage = "Dear " + name + "," + "Your Mobile No.: " + mobile + " & Password: " + password + " - For MoneyGiver App";


                    if (!validateEmail ()) {
                        return;
                    }
                    // long user_code = 3000;
                    // Generate an Auto Increment Key Code using a TextView
                    if (user_code <= 0){
                        user_code = 1; //--set default start value--
                    }
                    else {
                        user_code++;  //--or just increment it--
                    }
//                  editor.putLong ( "code",user_code ); //--save new value--
//                  editor.apply ();
                    userCode = String.valueOf ( user_code ); //--save new value--
                    password = (name.substring(0,3) + userCode.substring ( 1,4 ));

                    registration();
                }
            }
        } );

        tiluserEmail.getEditText ().addTextChangedListener(new MyTextWatcher(tiluserEmail));

        tiluserBirthday.getEditText ().setOnTouchListener ( new View.OnTouchListener () {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                gdatePickerdialog.show ();
                gdatePickerdialog.getButton ( DatePickerDialog.BUTTON_NEGATIVE ).setTextColor ( Color.RED );
                gdatePickerdialog.getButton ( DatePickerDialog.BUTTON_POSITIVE ).setTextColor ( Color.RED );
                return false;
            }
        } );

    }

    private boolean checkPermission(String sendSms) {
        int checkpermission= ContextCompat.checkSelfPermission(this,sendSms);
        return checkpermission== PackageManager.PERMISSION_GRANTED;
    }

    private void registration() {
     //register the user in firebase

    fAuth.createUserWithEmailAndPassword ( email,password )
        .addOnSuccessListener ( new OnSuccessListener<AuthResult> () {
            @Override
            public void onSuccess(AuthResult authResult) {

            String message = "Dear " + name +"\n"+"Your User Code: " + userCode+"\n"+"Your Registered Mobile No.: " + mobile +"\n"
                    + "Password: " + password +"\n"+"Visit our website - www.moneygiver.in"+"\n"+ "Thanks and Regards"+"\n"+"MONEY GIVER";

            // Snack Bar to show success message that record saved successfully
            Snackbar.make ( scrollView, getString ( R.string.success_message ), Snackbar.LENGTH_LONG ).show ();
                Log.i ( "TAG", "Registration Successful: " );
            startActivity ( new Intent ( UserRegistration_Activity.this, Userlogin_Activity.class ) );

            //Send Mail
            JavaMailAPI javaMailAPI = new JavaMailAPI(UserRegistration_Activity.this, email ,
                                                      "Login Details for Money Giver App User Panel", message);
            javaMailAPI.execute();

            String smsmessage = "Dear " + name + "," + "Your Mobile No.: " + mobile +" & Password: " + password +" - For MoneyGiver App";


            //  emptyInputEditText();
            }
        } )
       .addOnFailureListener ( new OnFailureListener () {
           @Override
           public void onFailure(@NonNull Exception e) {
               Log.i ( "Tag",":onFailure" );
               Toast.makeText ( UserRegistration_Activity.this, "Failed :"+e.getMessage (), Toast.LENGTH_SHORT ).show ();
           }
       } );
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode){
            case SEND_SMS_PERMISSION_REQ:
                if(grantResults.length>0 &&(grantResults[0]==PackageManager.PERMISSION_GRANTED))
                {
                    btnregsubmit.setEnabled(true);
                }
                break;
        }
    }

    private void setDateField(){

        Calendar newCalendar = Calendar.getInstance();

        gdatePickerdialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                SimpleDateFormat sd = new SimpleDateFormat("dd-MM-yyyy");
                final Date startDate = newDate.getTime();
                String fdate = sd.format(startDate);

                tiluserBirthday.getEditText ().setText(fdate);

            }
        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

        gdatePickerdialog.getDatePicker().setMaxDate(System.currentTimeMillis());
    }

    public void SpinnerGender(){
        // Creating adapter for spinner
        ArrayAdapter<CharSequence> myadapter = ArrayAdapter.createFromResource ( this,R.array.gender,android.R.layout.simple_spinner_item );

        // Drop down layout style - list view with radio button
        myadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spinner_gender.setAdapter(myadapter);

        // Spinner click listener
        spinner_gender.setOnItemSelectedListener( new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    // On selecting a spinner item
                    //  parent.getItemAtPosition ( position ).toString ();
                    //set color of drop down textview
                    ((TextView) parent.getChildAt ( 0 )).setTextColor ( Color.parseColor ( "#5CD108" ) );

                }
                else {
                    // On selecting a spinner item
                    String item = parent.getItemAtPosition ( position ).toString ();

                    //set color of drop down textview
                    ((TextView) parent.getChildAt ( 0 )).setTextColor ( Color.parseColor ( "#5CD108" ) );

                    // Showing selected spinner item
                    Toast.makeText ( parent.getContext (), "Selected: " + item, Toast.LENGTH_LONG ).show ();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // showToast(q.name + " unselected");
            }
        });
    }

    //Validation allow blank space after 4 number and characters in edit text for Adhar Card
    public void split_string(){

        //When an object of a type is attached to an Editable, its methods will be called when the text is changed.
        tiluserAdhar.getEditText ().addTextChangedListener ( new TextWatcher () {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                int inputlength = tiluserAdhar.getEditText ().getText ().toString ().length ();

                if (count<=inputlength && (inputlength==4 ||inputlength==9)){

                    tiluserAdhar.getEditText ().setText ( tiluserAdhar.getEditText ().getText ().toString () + " " );

                    int pos = tiluserAdhar.getEditText ().getText ().length ();

                    tiluserAdhar.getEditText ().setSelection ( pos );
                }
                else if (count>=inputlength && (inputlength==4 ||inputlength==9)){

                    tiluserAdhar.getEditText ().setText ( tiluserAdhar.getEditText ().getText ().toString ().substring ( 0,tiluserAdhar.getEditText ().getText ().toString ().length ()-1 ) );

                    int pos =tiluserAdhar.getEditText ().getText ().length ();

                    tiluserAdhar.getEditText ().setSelection ( pos );
                }
                count = tiluserAdhar.getEditText ().getText ().toString ().length ();
            }
        } );
    }
    //Validation allow only number and characters in edit text for PAN Card
    private void character_digits(){

        InputFilter filter = new InputFilter () {
            @Override
            public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {

                for (int i = start; i < end; i++){
                    if (!Character.isLetterOrDigit ( source.charAt ( i ) ))
                    {
                        tiluserPan.setError (  "Only letters and digits allowed" );
                        return "";
                    }
                }
                return null;
            }
        };
        tiluserPan.getEditText ().setFilters ( new InputFilter[]{ filter} );
    }

    public void registerValidation() {

        name = tiluserName.getEditText ().getText().toString().trim ();
        adress = tiluserAddress.getEditText ().getText().toString().trim ();
        mobile = tiluserMobile.getEditText ().getText().toString().trim ();
        birthday = tiluserBirthday.getEditText ().getText().toString().trim ();
        email = tiluserEmail.getEditText ().getText().toString().trim ();
        adhar = tiluserAdhar.getEditText ().getText().toString().trim ();
        pan = tiluserPan.getEditText ().getText().toString().trim ();
        Spnr_gender = spinner_gender.getSelectedItem ().toString ().trim ();

        Log.d ( "Tag",":registerValidation" );
            if (!name.isEmpty ()&&!adress.isEmpty ()&&!mobile.isEmpty ()&&!birthday.isEmpty () && !Spnr_gender.equals ( "Gender" )
                    &&!email.isEmpty ()&&!adhar.isEmpty ()&&!pan.isEmpty () && chkAgree.isChecked ()==true) {
                    if (!validateEmail ()) {
                          return;
                          }

                //register the user in firebase
                Log.d ( "Tag",":Pre" );
                fAuth.createUserWithEmailAndPassword ( email,password )
                        .addOnCompleteListener ( new OnCompleteListener<AuthResult> () {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                if (task.isSuccessful ()) {
                                    Log.i ( "Tag",":onComplete" );
                                    Toast.makeText ( UserRegistration_Activity.this, "User Created", Toast.LENGTH_SHORT ).show ();
                                    startActivity ( new Intent ( UserRegistration_Activity.this, Userlogin_Activity.class ) );
                                }
                                else{

                                    Log.i ( "Tag",":onError" );
                                    Toast.makeText ( UserRegistration_Activity.this, ""+task.getException ().getMessage (), Toast.LENGTH_SHORT ).show ();
                                }
                            }
                        } );

                            // Snack Bar to show success message that record saved successfully
                            Snackbar.make ( scrollView, getString ( R.string.success_message ), Snackbar.LENGTH_LONG ).show ();
                            emptyInputEditText();

                            sharedpref = getApplicationContext ().getSharedPreferences ( "value",0 );
                            SharedPreferences.Editor editor = sharedpref.edit ();
                           // long user_code = 3000;
                            // Generate an Auto Increment Key Code using a TextView
                            if (user_code <= 0){
                                user_code = 1; //--set default start value--
                            }
                            else {
                                user_code++;  //--or just increment it--
                            }

                            //  --use code variable now--
                           // String userCode;
                            userCode = String.valueOf ( user_code );
                            editor.putLong ( "code",user_code ); //--save new value--
                            editor.putString ("NAME", name );
                            editor.apply ();
                            password = (name.substring(0,3) + userCode.substring ( 0,3 ));
                            String message = "Dear " + name +"\n"+"Your Agent Code: " + userCode+"\n"+"Your Registered Mobile No.: " + mobile +"\n"+ "Password: " + password +"\n"+"Visit our website - www.moneygiver.in"+"\n"+
                                          "Thanks and Regards"+"\n"+"MONEY GIVER";
                            //Send Mail
                            JavaMailAPI javaMailAPI = new JavaMailAPI(this,email,"Login Details for Money Giver App User Panel",
                                    message);

                            javaMailAPI.execute();

                    } else if (name.equals ( "" )) {
                        Toast.makeText ( this, "Please Enter Name", Toast.LENGTH_SHORT ).show ();
                    } else if (adress.equals ( "" )) {
                        Toast.makeText ( this, "Please Enter Address", Toast.LENGTH_SHORT ).show ();
                    } else if (mobile.equals ( "" )) {
                        Toast.makeText ( this, "Please Enter Mobile Number", Toast.LENGTH_SHORT ).show ();
                    } else if (birthday.equals ( "" )) {
                        Toast.makeText ( this, "Please Enter Date of Birth", Toast.LENGTH_SHORT ).show ();
                    } else if (Spnr_gender.equals("Gender")) {
                        Toast.makeText(this, "Please Select Gender", Toast.LENGTH_SHORT).show();
                    } else if (email.equals ( "" )&& !Patterns.EMAIL_ADDRESS.matcher ( email ).matches ()) {
                        Toast.makeText ( this, "Enter Valid Email Address", Toast.LENGTH_SHORT ).show ();
                    } else if (adhar.equals ( "" )) {
                        Toast.makeText ( this, "Please Enter Adhar Number", Toast.LENGTH_SHORT ).show ();
                    } else if (pan.equals ( "" )) {
                        Toast.makeText ( this, "Please Enter Pan Number", Toast.LENGTH_SHORT ).show ();
                    } else if (chkAgree.isChecked () == false) {
                        Toast.makeText ( this, "Please Click Terms & Conditions", Toast.LENGTH_SHORT ).show ();
                    }
                    //hideKeyboardFrom(nestedScrollView);
    }
    private void emptyInputEditText() {
        tiluserName.getEditText ().setText(null);
        tiluserAddress.getEditText ().setText(null);
        tiluserMobile.getEditText ().setText(null);
        tiluserBirthday.getEditText ().setText(null);
        tiluserEmail.getEditText().setText (null);
        tiluserAdhar.getEditText ().setText(null);
        tiluserPan.getEditText ().setText(null);
    }

    private void hideKeyboardFrom(View view) {
        InputMethodManager imm = (InputMethodManager) this.getSystemService( Activity.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    private boolean validateEmail() {
        String email = tiluserEmail.getEditText ().getText().toString().trim();

        if (email.isEmpty() || !isValidEmail(email)) {
            tiluserEmail.setError(getString(R.string.error_message_email));
            requestFocus(tiluserEmail);
            return false;
        } else {
            tiluserEmail.setErrorEnabled(false);
        }

        return true;
    }

    public final static boolean isValidEmail(CharSequence email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }
    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode( WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    //When an object of this type is attached to an Editable, its methods will be called when the text is changed.
    private class MyTextWatcher implements TextWatcher{
        private View view;
        private MyTextWatcher(View view) {
            this.view = view;
        }
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }
        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }
        @Override
        public void afterTextChanged(Editable editable) {

            switch (view.getId()) {
                case R.id.tiluserEmail:
                    validateEmail();
                    break;
            }
        }
    }
}
