package com.example.moneygiver;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class Welcome_AgentActivity extends AppCompatActivity implements View.OnClickListener {

    Button btnViewUsers,btnCondition,btnchpass,btnlogout;
    TextView txtview_welcome;
    String string = "";
    SharedPreferences sharedPreferences;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate ( savedInstanceState );
        setContentView ( R.layout.activity_welcome_agent );

        txtview_welcome=findViewById ( R.id.txtview_welcome );
        btnViewUsers = findViewById(R.id.btnViewUsers);
        btnCondition = findViewById(R.id.btnCondition);
        btnchpass = findViewById(R.id.btnchpass);
        btnlogout = findViewById(R.id.btnlogout);

        sharedPreferences = getApplicationContext ().getSharedPreferences ( "value_agent",0 );
        String nm = sharedPreferences.getString ( "ANAME",null );
        txtview_welcome.setText ( "Welcome " + nm );

        //Apply setOnClickListener On Buttons
        btnViewUsers.setOnClickListener ( this );
        btnCondition.setOnClickListener ( this );
        btnchpass.setOnClickListener ( this );
        btnlogout.setOnClickListener ( this );

    }

    @Override
    public void onClick(View view) {
        switch (view.getId ()) {
            case R.id.btnViewUsers:
                startActivity ( new Intent ( this,Agent_ViewUserActivity.class ) );
                break;
            case R.id.btnCondition:
                startActivity ( new Intent ( this,TermsCondition_AgentActivity.class ) );
                break;
            case R.id.btnchpass:
                startActivity ( new Intent ( this,PassChange_AgentActivity.class ) );
                break;
            case R.id.btnlogout:
                startActivity ( new Intent ( this,Agentlogin_Activity.class ) );
                break;
        }
    }
}
