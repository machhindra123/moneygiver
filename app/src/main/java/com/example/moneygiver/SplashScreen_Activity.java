package com.example.moneygiver;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

public class SplashScreen_Activity extends AppCompatActivity {

    private final int SPLASH_DISPLAY_LENGTH = 1000;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate ( savedInstanceState );
        setContentView ( R.layout.activity_splash_screen_ );

        new Handler ( ).postDelayed ( new Runnable () {
            @Override
            public void run() {
                Intent intent = new Intent ( SplashScreen_Activity.this,MainActivity.class );
                startActivity ( intent );
                finish ();
            }
        },SPLASH_DISPLAY_LENGTH);
    }
}
