package com.example.moneygiver;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputLayout;

public class ForgotPass_AgentActivity extends AppCompatActivity implements View.OnClickListener {

    TextInputLayout tilUserEmail,tilUserMob;
    Button btnemail,btnsms;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate ( savedInstanceState );
        setContentView ( R.layout.activity_forgot_pass_agent );

        tilUserEmail = findViewById ( R.id.tilUserEmail );
        tilUserMob = findViewById ( R.id.tilUserMob );
        btnemail = findViewById ( R.id.btnemail );
        btnsms = findViewById ( R.id.btnsms );

        String tilemail =tilUserEmail.getEditText ().getText ().toString ().trim ();
        String tilmob =tilUserMob.getEditText ().getText ().toString ().trim ();

        btnemail.setOnClickListener ( this );
        btnsms.setOnClickListener ( this );
    }

    @Override
    public void onClick(View view) {
        switch (view.getId ()){
            case R.id.btnemail:
                Toast.makeText ( this, "Email", Toast.LENGTH_SHORT ).show ();
                break;
            case R.id.btnsms:
                Toast.makeText ( this, "sms", Toast.LENGTH_SHORT ).show ();
                break;
        }
    }
}

