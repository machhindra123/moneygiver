package com.example.moneygiver;

import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.TextView;

public class Agent_ViewUserActivity extends AppCompatActivity {

    TextView welcome;
    SharedPreferences sf;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate ( savedInstanceState );
        setContentView ( R.layout.activity_agent_view_user );

        welcome = findViewById ( R.id.welcome );
        sf = getApplicationContext ().getSharedPreferences ( "value_agent",0 );
        String na = sf.getString ( "ANAME",null );
        welcome.setText ( "Welcome " + na );
    }
}
