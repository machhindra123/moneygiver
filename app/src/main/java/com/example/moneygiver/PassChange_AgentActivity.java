package com.example.moneygiver;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;

import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputLayout;

public class PassChange_AgentActivity extends AppCompatActivity {

    TextView txtview_code,txtview_Name;
    TextInputLayout tilCurrentPass,tilPassNew;
    Button btn_submit;
    SharedPreferences sharepref;
    ScrollView scView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate ( savedInstanceState );
        setContentView ( R.layout.activity_pass_change_agent );
        scView = findViewById ( R.id.scView );
        txtview_code = findViewById ( R.id.txtview_code );
        txtview_Name = findViewById ( R.id.txtview_Name );
        tilCurrentPass = findViewById ( R.id.tilCurrentPass );
        tilPassNew = findViewById ( R.id.tilPassNew );

        btn_submit = findViewById ( R.id.btn_submit );
        sharepref = getApplicationContext ().getSharedPreferences ( "value_agent",0 );
        String code = String.valueOf ( sharepref.getLong ( "codeagent",0 ) );
        txtview_code.setText ( code );
        String n = sharepref.getString ( "ANAME",null );
        txtview_Name.setText ( n );
        btn_submit.setOnClickListener ( new View.OnClickListener () {
            @Override
            public void onClick(View view) {
                ChangePassword();
            }
        } );
    }

    private void ChangePassword() {
        String currentPass = tilCurrentPass.getEditText ().getText ().toString ().trim ();
        String newPass = tilPassNew.getEditText ().getText ().toString ().trim ();
        if (!currentPass.isEmpty () &&!newPass.isEmpty () ){
            Intent i = new Intent ( this,Agentlogin_Activity.class );
            startActivity ( i );
        }

        else if (currentPass.isEmpty ()){
            Snackbar.make ( scView,"Please enter current password",Snackbar.LENGTH_SHORT ).show ();
        }
        else if (newPass.isEmpty ()){
            Snackbar.make ( scView,"Enter a new password",Snackbar.LENGTH_SHORT ).show ();
        }
    }
}
