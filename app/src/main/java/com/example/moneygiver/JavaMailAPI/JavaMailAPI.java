package com.example.moneygiver.JavaMailAPI;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.sql.DataSource;

public class JavaMailAPI extends AsyncTask<Void,Void,Void>  {


    //Variables
    private Context mContext;
    private Session mSession;
    private String mEmail;
    private String mSubject;
    private String message;
//    private ProgressDialog mProgressDialog;

    //Constructor
    public JavaMailAPI(Context mContext, String mEmail, String mSubject, String message) {
        this.mContext = mContext;
        this.mEmail = mEmail;
        this.mSubject = mSubject;
        this.message = message;
    }
//
//    @Override
//    protected void onPreExecute() {
//        super.onPreExecute();
//        //Show progress dialog while sending email
//        mProgressDialog = ProgressDialog.show(mContext,"Sending message", "Please wait...",false,false);
//    }
//
//    @Override
//    protected void onPostExecute(Void aVoid) {
//        super.onPostExecute(aVoid);
//        //Dismiss progress dialog when message successfully send
//        mProgressDialog.dismiss();
//
//        //Show success toast
//        Toast.makeText(mContext,"Message Sent",Toast.LENGTH_SHORT).show();
//    }

    @Override
    protected Void doInBackground(Void... params) {
        //Creating properties
        Properties props = new Properties();

        //Configuring properties for gmail
        //If you are not using gmail you may need to change the values
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", "465");

        //Creating a new session  // Get the Session object.
        mSession = Session.getDefaultInstance(props,
                new Authenticator() {
                    //Authenticating the password
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(Utils.EMAIL, Utils.PASSWORD);
                    }
                });

        try {
            //Creating MimeMessage object
            MimeMessage mm = new MimeMessage(mSession);

            //Setting sender address
            mm.setFrom(new InternetAddress(Utils.EMAIL));
            //Adding receiver
            mm.addRecipient(Message.RecipientType.TO, new InternetAddress(mEmail));
            //Adding subject
            mm.setSubject(mSubject);
            //Adding message
            mm.setText ( message );

           // mm.setText(message + '\n' + mCode + '\n' + mMobile + '\n' + mPassword + '\n' + description );

            //Sending email
            Transport.send(mm);

            // Create the message part
//            BodyPart messageBodyPart = new MimeBodyPart ();
//            Now set the actual message
//            messageBodyPart.setText(message);
//            Create a multipart message
//            Multipart multipart = new MimeMultipart ();
//            Set text message part
//            multipart.addBodyPart(messageBodyPart);
//
//            messageBodyPart = new MimeBodyPart();
//
//            DataSource source = new FileDataSource (filePath);
//
//            messageBodyPart.setDataHandler(new DataHandler ( (javax.activation.DataSource) source ));
//
//            messageBodyPart.setFileName(filePath);
//
//            multipart.addBodyPart(messageBodyPart);
//            Send the complete message parts
//            mm.setContent(multipart);
 //           Send message
 //           Transport.send(mm);

        } catch (MessagingException e) {
            e.printStackTrace();
        }
        return null;
    }
}