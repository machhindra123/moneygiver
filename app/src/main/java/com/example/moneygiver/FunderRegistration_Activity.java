package com.example.moneygiver;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Patterns;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.moneygiver.JavaMailAPI.JavaMailAPI;
import com.example.moneygiver.Model.FunderModel;
import com.example.moneygiver.Model.UserModel;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputLayout;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class FunderRegistration_Activity extends AppCompatActivity {

    TextInputLayout tilFunderName,tilFunderAddress,tilFunderMobile,
                    tilFunderBirthday,tilFunderEmail,tilFunderAdhar,tilFunderPan;

    TextView txtview_fundercode;
    Button btn_funderreg;
    CheckBox chkbox_condition;
    DBHelper1 DB2;
    DatePickerDialog mDatePickerDialog;
    Spinner spinner_gender;
    String fun_code,fun_name,fun_adress,fun_mobile,fun_birthday,fun_email,fun_adhar,fun_pan,spner_gender;
    ScrollView scrollvw;
    SharedPreferences shapf;
    int count = 0;
    private String userCode;
    private String weburl = "Visit our website - www.moneygiver.in";
    private String thank = "Thanks and Regards";
    private String moneygiver = "MONEY GIVER";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_funder_registration_);

        txtview_fundercode=findViewById ( R.id.txtview_fundercode );
        tilFunderName=findViewById ( R.id.tilFunderName );
        tilFunderAddress=findViewById ( R.id.tilFunderAddress );
        tilFunderMobile=findViewById ( R.id.tilFunderMobile );
        tilFunderBirthday=findViewById ( R.id.tilFunderBirthday );
        tilFunderEmail=findViewById ( R.id.tilFunderEmail );
        tilFunderAdhar=findViewById ( R.id.tilFunderAdhar );
        tilFunderPan=findViewById ( R.id.tilFunderPan );
        spinner_gender = findViewById ( R.id.spinner_gender );

        chkbox_condition = findViewById(R.id.chkbox_condition);
        btn_funderreg = findViewById(R.id.btn_funderreg);

        scrollvw = findViewById(R.id.scrollvw);
        DB2 = new DBHelper1 ( this );
        tilFunderEmail.getEditText ().addTextChangedListener(new MyTextWatcher (tilFunderEmail));

        setDateTimeField();
        Spinner_Gender();
        split_string();
        character_digits();
        tilFunderBirthday.getEditText ().setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                mDatePickerDialog.show();
                mDatePickerDialog.getButton ( DatePickerDialog.BUTTON_NEGATIVE ).setTextColor ( Color.RED );
                mDatePickerDialog.getButton ( DatePickerDialog.BUTTON_POSITIVE ).setTextColor ( Color.RED );
                return false;
            }
        });
        btn_funderreg.setOnClickListener ( new View.OnClickListener () {
            @Override
            public void onClick(View view) {
                FunderRegister();
            }
        } );
    }

    private void Spinner_Gender() {
        ArrayAdapter<CharSequence> myadapter = ArrayAdapter.createFromResource ( this,R.array.gender,android.R.layout.simple_spinner_item );

        // Drop down layout style - list view with radio button
        myadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spinner_gender.setAdapter(myadapter);

        // Spinner click listener
        spinner_gender.setOnItemSelectedListener( new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    // On selecting a spinner item
                    //  parent.getItemAtPosition ( position ).toString ();
                    //set color of drop down textview
                    ((TextView) parent.getChildAt ( 0 )).setTextColor ( Color.parseColor ( "#FA113A" ) );

                }
                else {
                    // On selecting a spinner item
                    String item = parent.getItemAtPosition ( position ).toString ();

                    //set color of drop down textview
                    ((TextView) parent.getChildAt ( 0 )).setTextColor ( Color.parseColor ( "#FA113A" ) );

                    // Showing selected spinner item
                    Toast.makeText ( parent.getContext (), "Selected: " + item, Toast.LENGTH_LONG ).show ();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // showToast(q.name + " unselected");
            }
        });
    }

    private void setDateTimeField() {

        Calendar newCalendar = Calendar.getInstance();

        mDatePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                Calendar newDate = Calendar.getInstance();

                newDate.set(year, monthOfYear, dayOfMonth);

                SimpleDateFormat sd = new SimpleDateFormat("dd-MM-yyyy");

                final Date startDate = newDate.getTime();

                String fdate = sd.format(startDate);

                tilFunderBirthday.getEditText ().setText(fdate);

            }
        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

        mDatePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
    }
    //Validation allow blank space after 4 number and characters in edit text for Adhar Card
    public void split_string(){

        //When an object of a type is attached to an Editable, its methods will be called when the text is changed.
        tilFunderAdhar.getEditText ().addTextChangedListener ( new TextWatcher () {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                int inputlength = tilFunderAdhar.getEditText ().getText ().toString ().length ();

                if (count<=inputlength && (inputlength==4 ||inputlength==9)){

                    tilFunderAdhar.getEditText ().setText ( tilFunderAdhar.getEditText ().getText ().toString () + " " );

                    int pos = tilFunderAdhar.getEditText ().getText ().length ();

                    tilFunderAdhar.getEditText ().setSelection ( pos );
                }
                else if (count>=inputlength && (inputlength==4 ||inputlength==9)){

                    tilFunderAdhar.getEditText ().setText ( tilFunderAdhar.getEditText ().getText ().toString ().substring ( 0,tilFunderAdhar.getEditText ().getText ().toString ().length ()-1 ) );

                    int pos =tilFunderAdhar.getEditText ().getText ().length ();

                    tilFunderAdhar.getEditText ().setSelection ( pos );
                }
                count = tilFunderAdhar.getEditText ().getText ().toString ().length ();
            }
        } );
    }
    //Validation allow only number and characters in edit text for PAN Card
    private void character_digits(){

        InputFilter filter = new InputFilter () {
            @Override
            public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {

                for (int i = start; i < end; i++){
                    if (!Character.isLetterOrDigit ( source.charAt ( i ) ))
                    {
                        tilFunderPan.getEditText ().setError (  "Only letters and digits allowed" );
                        return "";
                    }
                }
                return null;
            }
        };
        tilFunderPan.getEditText ().setFilters ( new InputFilter[]{ filter} );
    }

    private void FunderRegister() {
                 fun_name = tilFunderName.getEditText ().getText().toString();
                 fun_adress = tilFunderAddress.getEditText ().getText().toString();
                 fun_mobile = tilFunderMobile.getEditText ().getText().toString();
                 fun_birthday = tilFunderBirthday.getEditText ().getText().toString();
                 fun_email = tilFunderEmail.getEditText ().getText().toString();
                 fun_adhar = tilFunderAdhar.getEditText ().getText().toString();
                 fun_pan = tilFunderPan.getEditText ().getText().toString();
                 spner_gender = spinner_gender.getSelectedItem ().toString ().trim ();

                DB2 = new DBHelper1 ( FunderRegistration_Activity.this);

                if (!fun_name.isEmpty ()&&!fun_adress.isEmpty ()&&!fun_mobile.isEmpty ()&&!fun_birthday.isEmpty ()&&!spner_gender.equals ( "Gender" )
                    &&!fun_email.isEmpty ()&&!fun_adhar.isEmpty ()&&!fun_pan.isEmpty () && chkbox_condition.isChecked ()==true){

                    if (!validateEmail ()) {
                        return;
                    }

                    if (!DB2.checkUserFunder ( fun_email )) {

                        FunderModel funderModel = new FunderModel ();

                        funderModel.setFund_code (fun_code);
                        funderModel.setFund_name (fun_name);
                        funderModel.setFund_email (fun_email );
                        funderModel.setFund_adress (fun_adress);
                        funderModel.setFund_birthday (fun_birthday);
                        funderModel.setFund_mobile (fun_mobile);
//                        funderModel.setFund_sex ( String.valueOf ( selectedId ) );
                        funderModel.setFund_adhar (fun_adhar);
                        funderModel.setFund_pan (fun_pan);
                        DB2.addUser ( funderModel );

                        // Snack Bar to show success message that record saved successfully
                        Snackbar.make ( scrollvw, getString ( R.string.success_message ), Snackbar.LENGTH_LONG ).show ();
                        emptyInputEditText();
                        Intent inte = new Intent ( this, Funderlogin_Activity.class );
                        shapf = getApplicationContext ().getSharedPreferences ( "value_funder",0 );
                        SharedPreferences.Editor editor = shapf.edit ();
                         long user_code = 4000;
                        // Generate an Auto Increment Key Code using a TextView
                        if (user_code <= 0){
                            user_code = 1; //--set default start value--
                        }
                        else {
                            user_code++;  //--or just increment it--
                        }
                        editor.putLong ( "codefunder",user_code ); //--save new value--
                        //  --use code variable now--
                        // String userCode;
                        userCode = String.valueOf ( user_code );
                        editor.putString ("FNAME", fun_name );
                        editor.apply ();
                        String password=fun_name.substring(0,3);
                        String message = "Dear " + fun_name +"\n"+"Your User Code: " + userCode+"\n"+"Your Registered Mobile No.: " + fun_mobile +"\n"
                                + "Password: " + password +"\n"+"Visit our website - www.moneygiver.in"+"\n"+ "Thanks and Regards"+"\n"+"MONEY GIVER";
                        JavaMailAPI javaMailAPI = new JavaMailAPI(FunderRegistration_Activity.this, fun_email,
                                                                  "Login Details for Money Giver App Funder Panel", message );

                        javaMailAPI.execute();
                        startActivity ( inte );

                    } else {
                        // Snack Bar to show error message that record already exists
                        Snackbar.make ( scrollvw, getString ( R.string.error_email_exists ), Snackbar.LENGTH_LONG ).show ();
                    }
                }
                else if (fun_name.isEmpty ()){
                    Toast.makeText ( this, "Please Enter Name", Toast.LENGTH_SHORT ).show ();
                }
                else if (fun_adress.isEmpty ()){
                    Toast.makeText ( this, "Please Enter Address", Toast.LENGTH_SHORT ).show ();
                }
                else if (fun_mobile.isEmpty ()){
                    Toast.makeText ( this, "Please Enter Mobile Number", Toast.LENGTH_SHORT ).show ();
                }
                else if (fun_birthday.isEmpty ()){
                    Toast.makeText ( this, "Please Enter Date of Birth", Toast.LENGTH_SHORT ).show ();
                }
                else if (spner_gender.equals ( "Gender" )){
                    Toast.makeText ( this, "Please select Gender", Toast.LENGTH_SHORT ).show ();
                }
                else if (fun_email.isEmpty ()&&!Patterns.EMAIL_ADDRESS.matcher ( fun_email ).matches ()){
                    Toast.makeText ( this, "Please Enter Valid Email Address", Toast.LENGTH_SHORT ).show ();
                }
                else if (fun_adhar.isEmpty ()){
                    Toast.makeText ( this, "Please Enter Adhar Number", Toast.LENGTH_SHORT ).show ();
                }
                else if (fun_pan.isEmpty ()){
                    Toast.makeText ( this, "Please Enter Pan Number", Toast.LENGTH_SHORT ).show ();
                }
                else if (chkbox_condition.isChecked () == false){
                    Toast.makeText ( this, "Please Click Terms & Conditions", Toast.LENGTH_SHORT ).show ();
                }
    }
    private void emptyInputEditText() {
        tilFunderName.getEditText ().setText(null);
        tilFunderAddress.getEditText ().setText(null);
        tilFunderMobile.getEditText ().setText(null);
        tilFunderBirthday.getEditText ().setText(null);
        tilFunderEmail.getEditText ().setText(null);
        tilFunderAdhar.getEditText ().setText(null);
        tilFunderPan.getEditText ().setText ( null );
    }

    private void hideKeyboardFrom(View view) {
        InputMethodManager imm = (InputMethodManager) this.getSystemService( Activity.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    private boolean validateEmail() {
        String email = tilFunderEmail.getEditText ().getText().toString().trim();

        if (email.isEmpty() || !isValidEmail(email)) {
            tilFunderEmail.setError(getString(R.string.error_message_email));
            requestFocus(tilFunderEmail.getEditText ());
            return false;
        } else {
            tilFunderEmail.setErrorEnabled(false);
        }

        return true;
    }

    public final static boolean isValidEmail(CharSequence email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }
    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode( WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    //When an object of this type is attached to an Editable, its methods will be called when the text is changed.
    private class MyTextWatcher implements TextWatcher {

        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void afterTextChanged(Editable editable) {

            switch (view.getId()) {
                case R.id.tilFunderEmail:
                    validateEmail();
                    break;
            }
        }
    }
}
