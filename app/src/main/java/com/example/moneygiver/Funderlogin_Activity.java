package com.example.moneygiver;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputLayout;

public class Funderlogin_Activity extends AppCompatActivity implements View.OnClickListener {

    TextInputLayout tilFunderUser,tilFunderPass;
    EditText edt_fundermobNo,edt_funderPass;
    TextView txtvw_forgotPass,funder_register;
    Button btn_funderlogin;
    String fun_mob,fun_email,fun_user,fun_pass;
    DBHelper1 db;
    LinearLayout linearLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_funderlogin_);

        tilFunderUser=findViewById ( R.id.tilFunderUser );
        tilFunderPass=findViewById ( R.id.tilFunderPass );
        edt_fundermobNo=findViewById(R.id.edt_fundermobNo);
        edt_funderPass=findViewById(R.id.edt_funderPass);
        txtvw_forgotPass=findViewById(R.id.txtvw_forgotPass);
        funder_register=findViewById(R.id.funder_register);
        btn_funderlogin=findViewById(R.id.btn_funderlogin);

        funder_register.setOnClickListener ( this );
        btn_funderlogin.setOnClickListener ( this );
        txtvw_forgotPass.setOnClickListener ( this );
        db = new DBHelper1 ( this );
    }
    private void Funder_Login() {
      fun_user = edt_fundermobNo.getText().toString();
      fun_pass = edt_funderPass.getText().toString();

       if (!fun_user.equals("")&& !fun_pass.equals("")) {
         if (db.checkUserFunder ( fun_user, fun_pass )) {

           Intent accountsIntent = new Intent ( this, Welcome_FunderActivity.class );
           emptyInputEditText ();
           startActivity ( accountsIntent );
           } else if (!db.checkUserFunder ( fun_user, fun_pass )) {
           Toast.makeText ( this, "Enter correct User Name or Password", Toast.LENGTH_SHORT ).show ();
           } else {
           // Snack Bar to show success message that record is wrong
           Snackbar.make ( linearLayout, getString ( R.string.error_valid_email_password ), Snackbar.LENGTH_LONG ).show ();
           }
        }
         else {
           if (fun_user.equals ( "" )){
            Toast.makeText ( this,"Please Enter User Id",Toast.LENGTH_SHORT).show ();
            }
            else if (fun_pass.equals ( "" )){
            Toast.makeText ( this,"Please Enter Password",Toast.LENGTH_SHORT).show ();
            }
         }
    }
    /**
     * This method is to empty all input edit text
     */
    private void emptyInputEditText() {
        edt_fundermobNo.setText(null);
        edt_funderPass.setText(null);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId ()){
            case R.id.funder_register:
                startActivity(new Intent(this, FunderRegistration_Activity.class));
                break;
            case R.id.btn_funderlogin:
                //startActivity(new Intent(this, Welcome_FunderActivity.class));
                Funder_Login();
                break;
            case R.id.txtvw_forgotPass:
                startActivity ( new Intent ( this,ForgotPass_FunderActivity.class ) );
                break;
        }
    }
}
