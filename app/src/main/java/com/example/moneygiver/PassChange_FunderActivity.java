package com.example.moneygiver;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;

import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputLayout;

public class PassChange_FunderActivity extends AppCompatActivity {

    TextView txtview_code,txtview_Name;
    TextInputLayout tilCurrentPass,tilPassNew;
    EditText edt_currentPass,edt_newPass;
    Button btn_submit;
    ScrollView scView;
    SharedPreferences SharePrefe;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate ( savedInstanceState );
        setContentView ( R.layout.activity_pass_change_funder );

        scView = findViewById ( R.id.scView );
        txtview_code = findViewById ( R.id.txtview_code );
        txtview_Name = findViewById ( R.id.txtview_Name );
        tilCurrentPass = findViewById ( R.id.tilCurrentPass );
        tilPassNew = findViewById ( R.id.tilPassNew );
        edt_currentPass = findViewById ( R.id.edt_currentPass );
        edt_newPass = findViewById ( R.id.edt_newPass );
        btn_submit = findViewById ( R.id.btn_submit );

        SharePrefe = getApplicationContext ().getSharedPreferences ( "value_funder",0 );
        String code = String.valueOf ( SharePrefe.getLong ( "codefunder",0 ) );
        txtview_code.setText ( code );
        String nmf = SharePrefe.getString ( "FNAME",null );
        txtview_Name.setText ( nmf );

        btn_submit.setOnClickListener ( new View.OnClickListener () {
            @Override
            public void onClick(View view) {
                ChangePassword();
            }
        } );
    }
    private void ChangePassword() {
        String currentPass = edt_currentPass.getText ().toString ().trim ();
        String newPass = edt_newPass.getText ().toString ().trim ();

        if ( ! currentPass.isEmpty () && ! newPass.isEmpty ()){
              startActivity ( new Intent ( this,Funderlogin_Activity.class ) );
        }
        else if(currentPass.isEmpty ()){
            Snackbar.make ( scView,"Enter Current password",Snackbar.LENGTH_SHORT ).show ();
        }
        else if (newPass.isEmpty ()){
            Snackbar.make ( scView,"Enter new password",Snackbar.LENGTH_SHORT ).show ();
        }
    }
}
