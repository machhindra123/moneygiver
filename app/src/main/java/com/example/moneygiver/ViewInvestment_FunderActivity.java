package com.example.moneygiver;

import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.TextView;

public class ViewInvestment_FunderActivity extends AppCompatActivity {

    TextView txtview_welcome,txtview_viewUsers;
    SharedPreferences shapreference;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate ( savedInstanceState );
        setContentView ( R.layout.activity_view_investment_funder );

        txtview_welcome = findViewById ( R.id.txtview_welcome );
        txtview_viewUsers = findViewById ( R.id.txtview_viewUsers );

        shapreference = getApplicationContext ().getSharedPreferences ( "value_funder",0 );
        String nv = shapreference.getString ( "FNAME",null );
        txtview_welcome.setText ( "Welcome " + nv );
    }
}
