package com.example.moneygiver;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.widget.NestedScrollView;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.moneygiver.Model.UserModel;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class Userlogin_Activity extends AppCompatActivity implements View.OnClickListener {

    TextInputLayout tilUser,tilPass;
    EditText edt_userId,edt_userPass;
    TextView txtview_forgotPass,txtview_register;
    Button btn_userlogin;
    DBHelper1 db;
    UserModel userModel;
    ScrollView scrollview;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_);

        tilUser=findViewById ( R.id.tilUser );
        tilPass=findViewById ( R.id.tilPass );
        edt_userId= findViewById(R.id.edt_userId);
        edt_userPass=findViewById(R.id.edt_userPass);
        txtview_forgotPass=findViewById(R.id.txtview_forgotPass);
        txtview_register=findViewById(R.id.txtview_register);
        btn_userlogin=findViewById(R.id.btn_userlogin);

        txtview_register.setOnClickListener ( this );
        btn_userlogin.setOnClickListener ( this );
        txtview_forgotPass.setOnClickListener ( this );
        db = new DBHelper1(this);
        userModel = new UserModel ();
       // List<UserModel> userlist = db.getAllUser ();
    }
    public void login(){
        String user_Id = edt_userId.getText().toString().trim ();
        String user_Pass = edt_userPass.getText().toString().trim ();
        if (!user_Id.equals("") && !user_Pass.equals("")) {

            FirebaseAuth.getInstance ().signInWithEmailAndPassword ( user_Id, user_Pass )
                    .addOnSuccessListener ( new OnSuccessListener<AuthResult> () {
                        @Override
                        public void onSuccess(AuthResult authResult) {
                            //Snackbar.make(scrollview, "login Successful", Snackbar.LENGTH_LONG).show();
                            Toast.makeText(Userlogin_Activity.this, "login Successful", Toast.LENGTH_SHORT).show();

                            startActivity ( new Intent ( Userlogin_Activity.this,WelcomeActivity.class ) );
                        }
                    } )
                    .addOnFailureListener ( new OnFailureListener () {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Toast.makeText(Userlogin_Activity.this, "Failed"+e.getMessage (), Toast.LENGTH_SHORT).show();
                        }
                    } );

//            if (db.checkUser ( user_Id,user_Pass ) ) {
//
//                Intent i = new Intent(this, WelcomeActivity.class);
//                emptyInputEditText();
//                startActivity(i);
//            }
//            else if (!db.checkUser ( user_Id,user_Pass )) {
//                Toast.makeText(this, "Enter correct User Name or Password", Toast.LENGTH_SHORT).show();
//            }
//            else {
//                // Snack Bar to show success message that record is wrong
//                Snackbar.make(scrollview, getString(R.string.error_valid_email_password), Snackbar.LENGTH_LONG).show();
//            }
        }
        else{
            if (user_Id.equals ( "" )){
                Toast.makeText(this, "Please Enter User Name", Toast.LENGTH_SHORT).show();
            }
            else if (user_Pass.equals ( "" )){
                Toast.makeText(this, "Please Enter Password", Toast.LENGTH_SHORT).show();
            }
        }

      }
    /**
     * This method is to empty all input edit text
     */
    private void emptyInputEditText() {
        edt_userId.setText(null);
        edt_userPass.setText(null);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent i = new Intent(Userlogin_Activity.this, DBHelper1.class);
        startActivity(i);
        finish();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId ()){
            case R.id.txtview_register:
                startActivity(new Intent(this, UserRegistration_Activity.class));
                break;
            case R.id.btn_userlogin:
                //startActivity(new Intent(this, WelcomeActivity.class));
                login();
                break;
            case R.id.txtview_forgotPass:
                startActivity ( new Intent ( this,ForgotPassActivity.class ) );
                break;
        }
    }
}
