package com.example.moneygiver;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.material.textfield.TextInputLayout;

public class Payment_Details_Activity extends AppCompatActivity {

    TextInputLayout tilAgentName,tilAgentaddress,tilAgentbirthday,tilAgentmobile;
    EditText edt_name,edt_Address,edt_email,edt_phone;
    Button btn_submit;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate ( savedInstanceState );
        setContentView ( R.layout.activity_payment_details_ );

        tilAgentName = findViewById ( R.id.tilAgentName );
        tilAgentaddress = findViewById ( R.id.tilAgentaddress );
        tilAgentbirthday = findViewById ( R.id.tilAgentbirthday );
        tilAgentmobile = findViewById ( R.id.tilAgentmobile );
        edt_name = findViewById ( R.id.edt_name );
        edt_Address = findViewById ( R.id.edt_Address );
        edt_email = findViewById ( R.id.edt_email );
        edt_phone = findViewById ( R.id.edt_phone );
        btn_submit = findViewById ( R.id.btn_submit );

        Payment();
    }

    private void Payment() {
        String name = edt_name.getText ().toString ().trim ();
        String address = edt_Address.getText ().toString ().trim ();
        String email = edt_email.getText ().toString ().trim ();
        String phone = edt_phone.getText ().toString ().trim ();

        btn_submit.setOnClickListener ( new View.OnClickListener () {
            @Override
            public void onClick(View view) {
              //startActivity ( new Intent ( Payment_Details_Activity.this,PAYActivity.class ) );
            }
        } );
    }

}
