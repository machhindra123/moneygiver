package com.example.moneygiver;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputLayout;

public class LoanApplication_Activity extends AppCompatActivity {

    TextInputLayout tilAmount,tilUserLoanCode,tilAgentname;
    EditText edt_amount,edt_agentcode,edt_agentName;
    TextView txtview_LoadId,txtview_loanDate,txtview_userCode,txtview_userName;
    CheckBox check_condition;
    Button btncheck;
    DatePickerDialog gloandatePickerdialog;
    Spinner spinner1,spinner2;
    String loanAmount,agent_code,agent_name,loan_type,mortgage;
    SharedPreferences preferences;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loan_application_ );

        tilAmount=findViewById ( R.id.tilAmount );
        tilUserLoanCode=findViewById ( R.id.tilUserLoanCode );
        tilAgentname=findViewById ( R.id.tilAgentname );
        // Spinner element
        spinner1 = (Spinner) findViewById(R.id.spinner1);
        spinner2 = (Spinner) findViewById(R.id.spinner2);
       // Button button=(Button)findViewById(R.id.button);
        edt_amount = findViewById ( R.id.edt_amount );
        edt_agentcode = findViewById ( R.id.edt_agentcode );
        edt_agentName = findViewById(R.id.edt_agentName);
        txtview_LoadId = findViewById ( R.id.txtview_LoadId );
        txtview_loanDate = findViewById ( R.id.txtview_loanDate );
        txtview_userCode = findViewById ( R.id.txtview_userCode );
        txtview_userName = findViewById ( R.id.txtview_userName );
        check_condition = findViewById ( R.id.check_condition );
        btncheck = findViewById ( R.id.btncheck );

        preferences = getApplicationContext ().getSharedPreferences ( "value",0 );
        String code = String.valueOf ( preferences.getLong ( "code",0 ) );
        txtview_userCode.setText ( code );
        String nme = preferences.getString ( "NAME",null );
        txtview_userName.setText ( nme );

        btncheck.setOnClickListener ( new View.OnClickListener () {
            @Override
            public void onClick(View view) {

                inputValidationLoan();
            }
        } );

        //---------------------for 1st drop down list at spinner 1---------------
       ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource ( this,R.array.mortgage_type,android.R.layout.simple_spinner_item );
        // Drop down layout style - list view with radio button
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // Apply the adapter to the spinner
        spinner1.setAdapter(adapter);
        // Spinner click listener
        spinner1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    // On selecting a spinner item
                    parent.getItemAtPosition ( position ).toString ();
                    //set color of drop down textview
                    ((TextView) parent.getChildAt ( 0 )).setTextColor ( Color.parseColor ( "#5CD108" ) );
                }else {
                    // Your code to process the selection
                    // On selecting a spinner item
                    String item = parent.getItemAtPosition(position).toString();

                    //set color of drop down textview
                    ((TextView) parent.getChildAt(0)).setTextColor( Color.parseColor("#5CD108"));

                    // Showing selected spinner item
                    Toast.makeText(parent.getContext(), "Selected: " + item, Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // Another interface callback
            }
        });
    //---------------------for 2nd drop down list at spinner 2---------------

        // Creating adapter for spinner
        ArrayAdapter<CharSequence> myadapter = ArrayAdapter.createFromResource ( this,R.array.loan_type,android.R.layout.simple_spinner_item );

        // Drop down layout style - list view with radio button
        myadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spinner2.setAdapter(myadapter);

        // Spinner click listener
        spinner2.setOnItemSelectedListener( new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    // On selecting a spinner item
                   //  parent.getItemAtPosition ( position ).toString ();
                    //set color of drop down textview
                    ((TextView) parent.getChildAt ( 0 )).setTextColor ( Color.parseColor ( "#5CD108" ) );

                }
                else {
                    // On selecting a spinner item
                    String item = parent.getItemAtPosition ( position ).toString ();

                    //set color of drop down textview
                    ((TextView) parent.getChildAt ( 0 )).setTextColor ( Color.parseColor ( "#5CD108" ) );

                    // Showing selected spinner item
                    Toast.makeText ( parent.getContext (), "Selected: " + item, Toast.LENGTH_LONG ).show ();
                  }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
               // showToast(q.name + " unselected");
            }
        });

        /*loan_date.setOnTouchListener ( new View.OnTouchListener () {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {

                gloandatePickerdialog.show ();
                gloandatePickerdialog.getButton ( DatePickerDialog.BUTTON_NEGATIVE ).setTextColor ( Color.RED );
                gloandatePickerdialog.getButton ( DatePickerDialog.BUTTON_POSITIVE ).setTextColor ( Color.RED );
                return false;
            }
        } );*/
    }
   /* private void LoanDate(){

        Calendar newCalendar = Calendar.getInstance();

        gloandatePickerdialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();

                newDate.set(year, monthOfYear, dayOfMonth);

                SimpleDateFormat sd = new SimpleDateFormat("dd-MM-yyyy");

                final Date startDate = newDate.getTime();

                String fdate = sd.format(startDate);

                loan_date.setText(fdate);

            }
        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

        gloandatePickerdialog.getDatePicker().setMaxDate(System.currentTimeMillis());
    }*/
    //-----------------function for loan validation form-------------------------------
    private void inputValidationLoan(){

        loanAmount = edt_amount.getText().toString().trim ();
        agent_code = edt_agentcode.getText().toString().trim ();
        agent_name = edt_agentName.getText().toString().trim ();
        mortgage = spinner1.getSelectedItem ().toString ().trim ();
        loan_type = spinner2.getSelectedItem ().toString ().trim ();
        if (!loanAmount.isEmpty ()&&!agent_code.isEmpty ()&&!agent_name.isEmpty ()
              &&!mortgage.equals ( "Select Mortgage" ) &&!loan_type.equals ( "Select Loan" ) && check_condition.isChecked ()==true){

            Intent inte = new Intent ( this, Payment_Details_Activity.class );
            startActivity ( inte );
            finish ();
        }
        else if (loanAmount.isEmpty ()){
            Toast.makeText ( this, "Enter Amount", Toast.LENGTH_SHORT ).show ();
        }

        else if (mortgage.equals ( "Select Mortgage" )){
            Toast.makeText ( this, "Nothing Select Mortgage", Toast.LENGTH_SHORT ).show ();
        }
        else if (loan_type.equals ( "Select Loan" )){
            Toast.makeText ( this, "Nothing Select Loan", Toast.LENGTH_SHORT ).show ();
        }
        else if (agent_code.isEmpty ()){
            Toast.makeText ( this, "Enter Agent Code", Toast.LENGTH_SHORT ).show ();
        }
        else if (agent_name.isEmpty ()){
            Toast.makeText ( this, "Enter Agent Name", Toast.LENGTH_SHORT ).show ();
        }
        else if (check_condition.isChecked () == false){
            Toast.makeText ( this, "Please Click Terms & Conditions", Toast.LENGTH_SHORT ).show ();
        }
    }
}

