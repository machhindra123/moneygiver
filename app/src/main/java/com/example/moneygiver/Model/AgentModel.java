package com.example.moneygiver.Model;

public class AgentModel {
    private int agen_id;
    private String agen_code;
    private String agen_name;
    private String agen_adress;
    private String agen_mobile;
    private String agen_birthday;
    private int agen_sex;
    private String agen_email;
    private String agen_adhar;
    private String agen_pan;
    private String agen_bankname;
    private String agen_bankacc;
    private String agen_bankbranch;
    private String agen_bankifsc;

    public int getAgen_id() {
        return agen_id;
    }

    public void setAgen_id(int agen_id) {
        this.agen_id = agen_id;
    }

    public String getAgen_code() {
        return agen_code;
    }

    public void setAgen_code(String agen_code) {
        this.agen_code = agen_code;
    }

    public String getAgen_name() {
        return agen_name;
    }

    public void setAgen_name(String agen_name) {
        this.agen_name = agen_name;
    }

    public String getAgen_adress() {
        return agen_adress;
    }

    public void setAgen_adress(String agen_adress) {
        this.agen_adress = agen_adress;
    }

    public String getAgen_mobile() {
        return agen_mobile;
    }

    public void setAgen_mobile(String agen_mobile) {
        this.agen_mobile = agen_mobile;
    }

    public String getAgen_birthday() {
        return agen_birthday;
    }

    public void setAgen_birthday(String agen_birthday) {
        this.agen_birthday = agen_birthday;
    }

    public int getAgen_sex() {
        return agen_sex;
    }

    public void setAgen_sex(int agen_sex) {
        this.agen_sex = agen_sex;
    }

    public String getAgen_email() {
        return agen_email;
    }

    public void setAgen_email(String agen_email) {
        this.agen_email = agen_email;
    }

    public String getAgen_adhar() {
        return agen_adhar;
    }

    public void setAgen_adhar(String agen_adhar) {
        this.agen_adhar = agen_adhar;
    }

    public String getAgen_pan() {
        return agen_pan;
    }

    public void setAgen_pan(String agen_pan) {
        this.agen_pan = agen_pan;
    }

    public String getAgen_bankname() {
        return agen_bankname;
    }

    public void setAgen_bankname(String agen_bankname) {
        this.agen_bankname = agen_bankname;
    }

    public String getAgen_bankacc() {
        return agen_bankacc;
    }

    public void setAgen_bankacc(String agen_bankacc) {
        this.agen_bankacc = agen_bankacc;
    }

    public String getAgen_bankbranch() {
        return agen_bankbranch;
    }

    public void setAgen_bankbranch(String agen_bankbranch) {
        this.agen_bankbranch = agen_bankbranch;
    }

    public String getAgen_bankifsc() {
        return agen_bankifsc;
    }

    public void setAgen_bankifsc(String agen_bankifsc) {
        this.agen_bankifsc = agen_bankifsc;
    }
}
