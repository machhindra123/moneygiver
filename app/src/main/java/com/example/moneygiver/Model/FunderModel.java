package com.example.moneygiver.Model;

public class FunderModel {

    private int fund_id;
    private String fund_code;
    private String fund_name;
    private String fund_adress;
    private String fund_mobile;
    private String fund_birthday;
    private String fund_sex;
    private String fund_email;
    private String fund_adhar;
    private String fund_pan;

    public int getFund_id() {
        return fund_id;
    }

    public void setFund_id(int fund_id) {
        this.fund_id = fund_id;
    }

    public String getFund_code() {
        return fund_code;
    }

    public void setFund_code(String fund_code) {
        this.fund_code = fund_code;
    }

    public String getFund_name() {
        return fund_name;
    }

    public void setFund_name(String fund_name) {
        this.fund_name = fund_name;
    }

    public String getFund_adress() {
        return fund_adress;
    }

    public void setFund_adress(String fund_adress) {
        this.fund_adress = fund_adress;
    }

    public String getFund_mobile() {
        return fund_mobile;
    }

    public void setFund_mobile(String fund_mobile) {
        this.fund_mobile = fund_mobile;
    }

    public String getFund_birthday() {
        return fund_birthday;
    }

    public void setFund_birthday(String fund_birthday) {
        this.fund_birthday = fund_birthday;
    }

    public String getFund_sex() {
        return fund_sex;
    }

    public void setFund_sex(String fund_sex) {
        this.fund_sex = fund_sex;
    }

    public String getFund_email() {
        return fund_email;
    }

    public void setFund_email(String fund_email) {
        this.fund_email = fund_email;
    }

    public String getFund_adhar() {
        return fund_adhar;
    }

    public void setFund_adhar(String fund_adhar) {
        this.fund_adhar = fund_adhar;
    }

    public String getFund_pan() {
        return fund_pan;
    }

    public void setFund_pan(String fund_pan) {
        this.fund_pan = fund_pan;
    }
}
