package com.example.moneygiver;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Patterns;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.moneygiver.JavaMailAPI.JavaMailAPI;
import com.example.moneygiver.Model.AgentModel;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputLayout;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Agent_RegistrationActivity extends AppCompatActivity {

    TextInputLayout tilAgentname,tilAgentaddress,tilAgentmobile, tilAgentbirthday,tilAgentemail,tilAgentadhar,tilAgentpan,
                    tilbankname,tilbankacct,tilbankbranch,tilbankifsc;

    TextView txtview_agentcode;
    Button btn_regsubmit;
    CheckBox chkbox_condition;
    DBHelper1 DB3;
    DatePickerDialog mDatepickerDialog;
    ScrollView scrollview;
    Spinner spinner_gender;
    SharedPreferences shpref;
    int count =0;
    private String userCode;
    private String weburl = "Visit our website - www.moneygiver.in";
    private String thank = "Thanks and Regards";
    private String moneygiver = "MONEY GIVER";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agent_registration );

        scrollview = findViewById ( R.id.scrollview );
        txtview_agentcode = findViewById ( R.id.txtview_agentcode );

        tilAgentname=findViewById ( R.id.tilAgentname );
        tilAgentaddress=findViewById ( R.id.tilAgentaddress );
        tilAgentmobile=findViewById ( R.id.tilAgentmobile );
        tilAgentbirthday=findViewById ( R.id.tilAgentbirthday );
        tilAgentemail =findViewById ( R.id.tilAgentemail );
        tilAgentadhar=findViewById ( R.id.tilAgentadhar );
        tilAgentpan=findViewById ( R.id.tilAgentpan );
        tilbankname=findViewById ( R.id.tilbankname );
        tilbankacct=findViewById ( R.id.tilbankacct );
        tilbankbranch=findViewById ( R.id.tilbankbranch );
        tilbankifsc=findViewById ( R.id.tilbankifsc );
        spinner_gender = findViewById ( R.id.spinner_gender );


        chkbox_condition = findViewById(R.id.chkbox_condition);
        btn_regsubmit = findViewById(R.id.btn_regsubmit);

        DB3 = new DBHelper1 ( this );
        tilAgentemail.getEditText ().addTextChangedListener ( new MyTextWatcher ( tilAgentemail ) );

        setDate_Field();
        Spinner_Gender();
        split_string();
        character_digits();
        tilAgentbirthday.getEditText ().setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                mDatepickerDialog.show();
                mDatepickerDialog.getButton ( DatePickerDialog.BUTTON_NEGATIVE ).setTextColor ( Color.RED );
                mDatepickerDialog.getButton ( DatePickerDialog.BUTTON_POSITIVE ).setTextColor ( Color.RED );
                return false;
            }
        });
        btn_regsubmit.setOnClickListener ( new View.OnClickListener () {
            @Override
            public void onClick(View view) {
                Agent_Register();
            }
        } );
    }

    private void Spinner_Gender() {
        // Creating adapter for spinner
        ArrayAdapter<CharSequence> myadapter = ArrayAdapter.createFromResource ( this,R.array.gender,android.R.layout.simple_spinner_item );

        // Drop down layout style - list view with radio button
        myadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spinner_gender.setAdapter(myadapter);

        // Spinner click listener
        spinner_gender.setOnItemSelectedListener( new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    // On selecting a spinner item
                    //  parent.getItemAtPosition ( position ).toString ();
                    //set color of drop down textview
                    ((TextView) parent.getChildAt ( 0 )).setTextColor ( Color.parseColor ( "#FFBA5A" ) );

                }
                else {
                    // On selecting a spinner item
                    String item = parent.getItemAtPosition ( position ).toString ();

                    //set color of drop down textview
                    ((TextView) parent.getChildAt ( 0 )).setTextColor ( Color.parseColor ( "#FFBA5A" ) );

                    // Showing selected spinner item
                    Toast.makeText ( parent.getContext (), "Selected: " + item, Toast.LENGTH_LONG ).show ();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // showToast(q.name + " unselected");
            }
        });
    }

    private void setDate_Field() {

        Calendar newCalendar = Calendar.getInstance();
        mDatepickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                SimpleDateFormat sd = new SimpleDateFormat("dd-MM-yyyy");
                final Date startDate = newDate.getTime();
                String fdate = sd.format(startDate);

                tilAgentbirthday.getEditText ().setText(fdate);

            }
        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
        mDatepickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
    }
    //Validation allow blank space after 4 number and characters in edit text for Adhar Card
    public void split_string(){

        //When an object of a type is attached to an Editable, its methods will be called when the text is changed.
        tilAgentadhar.getEditText ().addTextChangedListener ( new TextWatcher () {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                int inputlength = tilAgentadhar.getEditText ().getText ().toString ().length ();

                if (count<=inputlength && (inputlength==4 ||inputlength==9)){

                    tilAgentadhar.getEditText ().setText ( tilAgentadhar.getEditText ().getText ().toString () + " " );

                    int pos = tilAgentadhar.getEditText ().getText ().length ();

                    tilAgentadhar.getEditText ().setSelection ( pos );
                }
                else if (count>=inputlength && (inputlength==4 ||inputlength==9)){

                    tilAgentadhar.getEditText ().setText ( tilAgentadhar.getEditText ().getText ().toString ().substring ( 0,tilAgentadhar.getEditText ().getText ().toString ().length ()-1 ) );

                    int pos =tilAgentadhar.getEditText ().getText ().length ();

                    tilAgentadhar.getEditText ().setSelection ( pos );
                }
                count = tilAgentadhar.getEditText ().getText ().toString ().length ();
            }
        } );
    }
    //Validation allow only number and characters in edit text for PAN Card
    private void character_digits(){

        InputFilter filter = new InputFilter () {
            @Override
            public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {

                for (int i = start; i < end; i++){
                    if (!Character.isLetterOrDigit ( source.charAt ( i ) ))
                    {
                        tilAgentpan.getEditText ().setError (  "Only letters and digits allowed" );
                        return "";
                    }
                }
                return null;
            }
        };
        tilAgentpan.getEditText ().setFilters ( new InputFilter[]{ filter} );
    }

    private void Agent_Register(){
                String ag_name = tilAgentname.getEditText ().getText().toString().trim ();
                String ag_adress = tilAgentaddress.getEditText ().getText().toString().trim ();
                String ag_mobile = tilAgentmobile.getEditText ().getText().toString().trim ();
                String ag_birthday = tilAgentbirthday.getEditText ().getText().toString().trim ();
                String ag_email = tilAgentemail.getEditText ().getText().toString().trim ();
                String ag_adhar = tilAgentadhar.getEditText ().getText().toString().trim ();
                String ag_pan = tilAgentpan.getEditText ().getText().toString().trim ();
                String ag_bankname = tilbankname.getEditText ().getText().toString().trim ();
                String ag_bankacc = tilbankacct.getEditText ().getText().toString().trim ();
                String ag_bankbranch = tilbankbranch.getEditText ().getText().toString().trim ();
                String ag_bankifsc = tilbankifsc.getEditText ().getText().toString().trim ();
                String Spnr_gender = spinner_gender.getSelectedItem ().toString ().trim ();

                DB3 = new DBHelper1 ( Agent_RegistrationActivity.this );
         if (!ag_name.isEmpty ()&&!ag_adress.isEmpty ()&&!ag_mobile.isEmpty ()&&!ag_birthday.isEmpty () && !Spnr_gender.equals ( "Gender" )
              &&!ag_email.isEmpty ()&&!ag_adhar.isEmpty ()&&!ag_pan.isEmpty () && !ag_bankname.isEmpty ()
              && !ag_bankacc.isEmpty () && !ag_bankbranch.isEmpty () && !ag_bankifsc.isEmpty ()&& chkbox_condition.isChecked ()==true){

                    if (!validateEmail ()) {
                        return;
                    }
                    if (!DB3.checkUserAgent ( ag_email )) {

                        AgentModel agentModel = new AgentModel ();

//                        agentModel.setAgen_code (  ag_code );
                        agentModel.setAgen_name ( ag_name );
                        agentModel.setAgen_adress ( ag_adress );
                        agentModel.setAgen_mobile ( ag_mobile );
                        agentModel.setAgen_birthday ( ag_birthday );
//                        agentModel.setAgen_sex ( selectedId );
                        agentModel.setAgen_email ( ag_email );
                        agentModel.setAgen_adhar ( ag_adhar );
                        agentModel.setAgen_pan ( ag_pan );
                        agentModel.setAgen_bankname ( ag_bankname );
                        agentModel.setAgen_bankacc ( ag_bankacc );
                        agentModel.setAgen_bankbranch ( ag_bankbranch );
                        agentModel.setAgen_bankifsc ( ag_bankifsc );
                        DB3.addUser ( agentModel );

                        // Snack Bar to show success message that record saved successfully
                        Snackbar.make ( scrollview, getString ( R.string.success_message ), Snackbar.LENGTH_LONG ).show ();
                        emptyInputEditText();

                        Intent inte = new Intent ( this, Agentlogin_Activity.class );

                        shpref = getApplicationContext ().getSharedPreferences ( "value_agent",0 );
                        SharedPreferences.Editor editor = shpref.edit ();
                        editor.putString ( "ANAME",ag_name );
                         long user_code = 3000;
                        // Generate an Auto Increment Key Code using a TextView
                        if (user_code <= 0){
                            user_code = 1; //--set default start value--
                        }
                        else {
                            user_code++;  //--or just increment it--
                        }
                        editor.putLong ( "codeagent",user_code ); //--save new value--
                        //  --use code variable now--
                        // String userCode;
                        userCode = String.valueOf ( user_code );
                        editor.apply ();
                        String password = ag_name.substring(0,3);
                        String message = "Dear " + ag_name +"\n"+"Your User Code: " + userCode+"\n"+"Your Registered Mobile No.: " + ag_mobile +"\n"
                                + "Password: " + password +"\n"+"Visit our website - www.moneygiver.in"+"\n"+ "Thanks and Regards"+"\n"+"MONEY GIVER";

                        JavaMailAPI javaMailAPI = new JavaMailAPI(Agent_RegistrationActivity.this, ag_email,
                                                                  "Login Details for Money Giver App Agent Panel", message );

                        javaMailAPI.execute();
                        startActivity ( inte );

                    } else {
                        // Snack Bar to show error message that record already exists
                        Snackbar.make ( scrollview, getString ( R.string.error_email_exists ), Snackbar.LENGTH_LONG ).show ();
                    }
                }
               else if (ag_name.equals("")) {
                    Toast.makeText(this, "Please Enter Name", Toast.LENGTH_SHORT).show();
                }
                else if (ag_adress.equals("")) {
                    Toast.makeText(this, "Please Enter Address", Toast.LENGTH_SHORT).show();
                }
                else if (ag_mobile.equals("")) {
                    Toast.makeText(this, "Please Enter Mobile Number", Toast.LENGTH_SHORT).show();
                }
                else if (ag_birthday.equals("")) {
                    Toast.makeText(this, "Please Enter Date of Birth", Toast.LENGTH_SHORT).show();
                }
                else if (Spnr_gender.equals("Gender")) {
                    Toast.makeText(this, "Please Select Gender", Toast.LENGTH_SHORT).show();
                }
                else if (ag_email.equals ( "" )) {
                    Toast.makeText(this, "Please Enter Valid Email Address", Toast.LENGTH_SHORT).show();
                }
                else if (ag_adhar.equals("")) {
                    Toast.makeText(this, "Please Enter Adhar Number", Toast.LENGTH_SHORT).show();
                }
                else if (ag_pan.equals("")) {
                    Toast.makeText(this, "Please Enter Pan Number", Toast.LENGTH_SHORT).show();
                }
                else if (ag_bankname.equals("")) {
                    Toast.makeText(this, "Please Enter Bank Name", Toast.LENGTH_SHORT).show();
                }
                else if (ag_bankacc.equals("")) {
                    Toast.makeText(this, "Please Enter Bank Account", Toast.LENGTH_SHORT).show();
                }
                else if (ag_bankbranch.equals("")) {
                    Toast.makeText(this, "Please Enter Bank Branch", Toast.LENGTH_SHORT).show();
                }
                else if (ag_bankifsc.equals("")) {
                    Toast.makeText(this, "Please Enter Bank IFSC Number", Toast.LENGTH_SHORT).show();
                }
                else if (chkbox_condition.isChecked() == false) {
                    Toast.makeText(this, "Please Click Terms & Conditions", Toast.LENGTH_SHORT).show();
                }
    }
    private boolean validateEmail() {
        String email = tilAgentemail.getEditText ().getText().toString().trim();

        if (email.isEmpty() || !isValidEmail(email)) {
            tilAgentemail.setError(getString(R.string.error_message_email));
            requestFocus(tilAgentemail.getEditText ());
            return false;
        } else {
            tilAgentemail.setErrorEnabled(false);
        }

        return true;
    }
    private void emptyInputEditText() {
        tilAgentname.getEditText ().setText(null);
        tilAgentaddress.getEditText ().setText ( null );
        tilAgentmobile.getEditText ().setText(null);
        tilAgentbirthday.getEditText ().setText(null);
        tilAgentemail.getEditText ().setText(null);
        tilAgentadhar.getEditText ().setText(null);
        tilAgentpan.getEditText ().setText(null);
        tilbankname.getEditText ().setText(null);
        tilbankacct.getEditText ().setText(null);
        tilbankbranch.getEditText ().setText(null);
        tilbankifsc.getEditText ().setText(null);
    }
    private void hideKeyboardFrom(View view) {
        InputMethodManager imm = (InputMethodManager) this.getSystemService( Activity.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }
    public final static boolean isValidEmail(CharSequence email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }
    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode( WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    //When an object of this type is attached to an Editable, its methods will be called when the text is changed.
    private class MyTextWatcher implements TextWatcher{

        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void afterTextChanged(Editable editable) {

            switch (view.getId()) {
                case R.id.tilAgentemail:
                    validateEmail();
                    break;
            }
        }
    }
}
